package com.tech.jaxoftrades.fragments.DashboardTabs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.utils.TimePicker;

import java.util.ArrayList;

/**
 * Created by arbaz on 25/5/17.
 */

public class AvailabilityFragment extends RootFragment {


    private com.tech.jaxoftrades.utils.TimePicker edAvailStartTime;
    private com.tech.jaxoftrades.utils.TimePicker edAvailEndTime;

    LinearLayout llDndMode, llDndModeLayout, llDndModeTitle;
    ArrayList<String> stringArrayList;
    TextView tvDndNote;

    public AvailabilityFragment() {
        // Required empty public constructor
    }

    public static AvailabilityFragment newInstance(int columnCount) {
        AvailabilityFragment fragment = new AvailabilityFragment();
        Bundle args = new Bundle();

//  args.putSerializable();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_availability, container, false);
        this.edAvailEndTime = (TimePicker) view.findViewById(R.id.edAvailEndTime);
        this.edAvailStartTime = (TimePicker) view.findViewById(R.id.edAvailStartTime);

        llDndMode = (LinearLayout) view.findViewById(R.id.llDndMode);
        llDndModeTitle = (LinearLayout) view.findViewById(R.id.llDndModeTitle);
        tvDndNote = (TextView) view.findViewById(R.id.tvDndNote);
        stringArrayList = new ArrayList<>();
        stringArrayList.add("");
        stringArrayList.add("");
        if (stringArrayList.size() > 0) {
            llDndMode.setVisibility(View.VISIBLE);
            llDndModeTitle.setVisibility(View.VISIBLE);
            for (int i = 0; i < stringArrayList.size(); i++) {
                llDndModeLayout = (LinearLayout) View.inflate(getActivity(), R.layout.dnd_layout_row, null);
                ((TextView) llDndModeLayout.findViewById(R.id.tvJobTitle)).setText(getString(R.string.defaultText));
                ((Switch) llDndModeLayout.findViewById(R.id.switchJobTitle)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            tvDndNote.setVisibility(View.VISIBLE);
                            AppDialog.showAlertDialog(getActivity(), null, getString(R.string.TmDbDndDialog),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            tvDndNote.setVisibility(View.GONE);
                            AppDialog.showAlertDialog(getActivity(), null, getString(R.string.TmDbDndDialogOff),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });
                llDndMode.addView(llDndModeLayout);

            }

        } else {
            llDndMode.setVisibility(View.GONE);
            llDndModeTitle.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }
}