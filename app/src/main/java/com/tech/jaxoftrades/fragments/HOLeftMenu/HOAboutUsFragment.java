package com.tech.jaxoftrades.fragments.HOLeftMenu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tech.jaxoftrades.R;


public class HOAboutUsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_ho_about_us, container, false);

        WebView wvAbtUs = (WebView) rootView.findViewById(R.id.wvAboutUs);

        wvAbtUs.loadUrl("http://www.jaxoftrades.com/#/about-us");

        WebSettings webSettings = wvAbtUs.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvAbtUs.setWebViewClient(new WebViewClient());

        return rootView;

    }

}
