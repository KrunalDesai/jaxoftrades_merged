package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.RecentProjectDetailActivity;
import com.tech.jaxoftrades.adapter.ProjectAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.listener.RecyclerItemClickListener;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class ProjectsFragment extends RootFragment implements View.OnClickListener {

    RecyclerView rvProjects;
    ImageView ivAddProject;
    ArrayList<String> stringArrayList;
    ProjectAdapter projectAdapter;

    public ProjectsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_projects, container, false);
        rvProjects = (RecyclerView) view.findViewById(R.id.rvProjects);
        ivAddProject = (ImageView) view.findViewById(R.id.ivAddProject);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        rvProjects.setLayoutManager(mLayoutManager1);


        stringArrayList = new ArrayList<>();
        stringArrayList.add("Test Data");
        stringArrayList.add("Test Data");
        projectAdapter = new ProjectAdapter(stringArrayList, getActivity());
        rvProjects.setAdapter(projectAdapter);
        ivAddProject.setOnClickListener(this);


        rvProjects.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        startActivity(new Intent(getActivity(), RecentProjectDetailActivity.class));
                    }
                }));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMRecentProjects));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAddProject:
                stringArrayList.add("");
                projectAdapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }
}
