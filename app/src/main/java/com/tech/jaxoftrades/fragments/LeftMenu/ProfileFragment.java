package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileAboutYouActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileContactInfoActivity;
import com.tech.jaxoftrades.adapter.ProfileListAdapter;
import com.tech.jaxoftrades.adapter.ProfileTabAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.model.Common.ProfileListMain;
import com.tech.jaxoftrades.model.Common.ProfileTabMain;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class ProfileFragment extends RootFragment implements View.OnClickListener {

    TextView tbSave;

    //Related To Adapter
    ProfileListAdapter profileListAdapter;
    ProfileListMain profileListMain;
    ArrayList<ProfileListMain> profileListMainArrayList;

    //Related To Activity
    RecyclerView rvProfileList;
    TabLayout tabsProfile;
    ViewPager tabsProfileViewPager;
    ProfileTabAdapter profileTabAdapter;
    ArrayList<ProfileTabMain> profileTabMainArrayList;
    LinearLayout llAboutYou,
            llContactInfo;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_profile, container, false);
//        rvProfileList = (RecyclerView) view.findViewById(R.id.rvProfileList);
//        rvProfileList.setLayoutManager(new LinearLayoutManager(getActivity()));

        tabsProfile = (TabLayout) view.findViewById(R.id.tabsProfile);
        tabsProfileViewPager = (ViewPager) view.findViewById(R.id.tabsProfileViewPager);
        llAboutYou = (LinearLayout) view.findViewById(R.id.llAboutYou);
        llContactInfo = (LinearLayout) view.findViewById(R.id.llContactInfo);


        profileTabMainArrayList = new ArrayList<>();
        profileTabMainArrayList.add(new ProfileTabMain("Plumber"));
        profileTabMainArrayList.add(new ProfileTabMain("Carpenter"));
        profileTabMainArrayList.add(new ProfileTabMain("Electrician"));
        profileTabMainArrayList.add(new ProfileTabMain("Painter"));


        //set Tabs ViewPager
        profileTabAdapter = new ProfileTabAdapter(getActivity(), getChildFragmentManager(), profileTabMainArrayList);
        tabsProfileViewPager.setAdapter(profileTabAdapter);
        tabsProfileViewPager.setOffscreenPageLimit(1);
        tabsProfile.setupWithViewPager(tabsProfileViewPager);


        llAboutYou.setOnClickListener(this);
        llContactInfo.setOnClickListener(this);



        /*profileListMainArrayList = new ArrayList<>();
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProSubCat), R.drawable.ic_subcategory_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProPlumbingTxt), R.drawable.ic_company_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProContactInfo), R.drawable.ic_phone_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProAboutYou), R.drawable.ic_about_you_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProSkills), R.drawable.ic_skills_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProDefaultTime), R.drawable.ic_default_time_avilability_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProDefaultLocation), R.drawable.ic_location_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProSocailMedia), R.drawable.ic_connect_social_media_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProAdvertiseProfile), R.drawable.ic_advertise_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProAddTrad), R.drawable.ic_add_trade_profile));
        profileListMainArrayList.add(new ProfileListMain(getString(R.string.TmProWriteTestimonial), R.drawable.ic_write_testimonial_profile));
*/
//        profileListAdapter = new ProfileListAdapter(profileListMainArrayList, getContext());
//        rvProfileList.setAdapter(profileListAdapter);

//        rvProfileList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                profileListMain = profileListMainArrayList.get(position);
//                try {
//                    if (profileListMain.getItemName() != null && !profileListMain.getItemName().isEmpty()) {
//                        if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProSubCat))) {
//                            startActivity(new Intent(getActivity(), ProfileSubcategoriesActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProPlumbingTxt))) {
//                            startActivity(new Intent(getActivity(), ProfileCompanyActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProContactInfo))) {
//                            startActivity(new Intent(getActivity(), ProfileContactInfoActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProAboutYou))) {
//                            startActivity(new Intent(getActivity(), ProfileAboutYouActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProSkills))) {
//                            startActivity(new Intent(getActivity(), ProfileSkillsActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProDefaultTime))) {
//                            startActivity(new Intent(getActivity(), ProfileDefaultTimeActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProDefaultLocation))) {
//                            startActivity(new Intent(getActivity(), ProfileDefaultLocationActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProSocailMedia))) {
//                            startActivity(new Intent(getActivity(), ProfileSocailMediaActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProAdvertiseProfile))) {
//                            startActivity(new Intent(getActivity(), ProfileAdvertiseActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProAddTrad))) {
//                            startActivity(new Intent(getActivity(), ProfileAddTradeActivity.class));
//                        } else if (profileListMain.getItemName().equalsIgnoreCase(getResources().getString(R.string.TmProWriteTestimonial))) {
//                            startActivity(new Intent(getActivity(), ProfileWriteTestimoniActivity.class));
//                        }
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMProfile));
        tbSave = displaySave();
        tbSave.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSave();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbSave:
                Toast.makeText(getActivity(), "Save Click", Toast.LENGTH_SHORT).show();
                break;
            case R.id.llAboutYou:
                startActivity(new Intent(getActivity(), ProfileAboutYouActivity.class));
                break;
            case R.id.llContactInfo:
                startActivity(new Intent(getActivity(), ProfileContactInfoActivity.class));
                break;
            default:
                break;
        }
    }

    public void removeTab(int position) {
        if (tabsProfile.getTabCount() >= 1 && position < tabsProfile.getTabCount()) {
            tabsProfile.removeTabAt(position);
            profileTabAdapter.removeTabPage(position);
        }
    }
}
