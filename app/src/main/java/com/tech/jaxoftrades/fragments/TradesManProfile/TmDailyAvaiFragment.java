package com.tech.jaxoftrades.fragments.TradesManProfile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.utils.TimePicker;

/**
 * Created by arbaz on 22/5/17.
 */

public class TmDailyAvaiFragment extends RootFragment implements View.OnClickListener {


    private com.tech.jaxoftrades.utils.TimePicker edAvailStartTime;
    private com.tech.jaxoftrades.utils.TimePicker edAvailEndTime;

    public TmDailyAvaiFragment() {
    }

    public static TmDailyAvaiFragment newInstance(int columnCount) {
        TmDailyAvaiFragment fragment = new TmDailyAvaiFragment();
        Bundle args = new Bundle();

//  args.putSerializable();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tm_daily_avai, container, false);
        this.edAvailEndTime = (TimePicker) view.findViewById(R.id.edAvailEndTime);
        this.edAvailStartTime = (TimePicker) view.findViewById(R.id.edAvailStartTime);


        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edAvailStartTime:
                break;
            case R.id.edAvailEndTime:
                break;
            default:
                break;
        }

    }
}
