package com.tech.jaxoftrades.fragments.Profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileAdvertiseActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileCompanyActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileDefaultLocationActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileDefaultTimeActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileSkillsActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileSocailMediaActivity;
import com.tech.jaxoftrades.activities.ProfileMenus.ProfileSubcategoriesActivity;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.utils.TimePicker;

import java.util.ArrayList;

/**
 * Created by arbaz on 25/5/17.
 */

public class ProfileTabFragment extends RootFragment implements View.OnClickListener {


    private TimePicker edAvailStartTime;
    private TimePicker edAvailEndTime;

    LinearLayout llDndMode, llDndModeLayout, llDndModeTitle;
    ArrayList<String> stringArrayList;
    TextView tvDndNote;
    LinearLayout llCompanyName,
            llSubCat,
            llSkills,
            llDailyAvaTime,
            llDefLocation,
            llSocial,
            llAdvertise,
            llRemoveTade;

    public ProfileTabFragment() {
        // Required empty public constructor
    }

    public static ProfileTabFragment newInstance(int columnCount) {
        ProfileTabFragment fragment = new ProfileTabFragment();
        Bundle args = new Bundle();


        //  args.putSerializable();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.profile_tab_layout, container, false);
        llCompanyName = (LinearLayout) view.findViewById(R.id.llCompanyName);
        llSubCat = (LinearLayout) view.findViewById(R.id.llSubCat);
        llSkills = (LinearLayout) view.findViewById(R.id.llSkills);
        llDailyAvaTime = (LinearLayout) view.findViewById(R.id.llDailyAvaTime);
        llDefLocation = (LinearLayout) view.findViewById(R.id.llDefLocation);
        llSocial = (LinearLayout) view.findViewById(R.id.llSocial);
        llAdvertise = (LinearLayout) view.findViewById(R.id.llAdvertise);
        llRemoveTade = (LinearLayout) view.findViewById(R.id.llRemoveTade);
        llCompanyName.setOnClickListener(this);
        llSubCat.setOnClickListener(this);
        llSkills.setOnClickListener(this);
        llDailyAvaTime.setOnClickListener(this);
        llDefLocation.setOnClickListener(this);
        llSocial.setOnClickListener(this);
        llAdvertise.setOnClickListener(this);
        llRemoveTade.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCompanyName:
                startActivity(new Intent(getActivity(), ProfileCompanyActivity.class));
                break;
            case R.id.llSubCat:
                startActivity(new Intent(getActivity(), ProfileSubcategoriesActivity.class));
                break;
            case R.id.llSkills:
                startActivity(new Intent(getActivity(), ProfileSkillsActivity.class));
                break;
            case R.id.llDailyAvaTime:
                startActivity(new Intent(getActivity(), ProfileDefaultTimeActivity.class));
                break;
            case R.id.llDefLocation:
                startActivity(new Intent(getActivity(), ProfileDefaultLocationActivity.class));
                break;
            case R.id.llSocial:
                startActivity(new Intent(getActivity(), ProfileSocailMediaActivity.class));
                break;
            case R.id.llAdvertise:
                startActivity(new Intent(getActivity(), ProfileAdvertiseActivity.class));
                break;
            case R.id.llRemoveTade:
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AppDialog.showAlertDialog(getActivity(), null, getString(R.string.TmProRemoveDialogMessage), getString(R.string.txt_remove), getString(R.string.txt_cancel), onPositiveClickListener, onNegativeClickListener);
                    }
                });

                break;
            default:
                break;
        }
    }
    /**
     * Set positive click listener for dialog
     */
    public DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            dialogInterface.dismiss();
        }
    };
    /**
     * Set Negative click listener for dialog
     */
    public DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            dialogInterface.dismiss();
        }
    };

}