package com.tech.jaxoftrades.fragments.TradesManProfile;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.model.Common.TagSkills;
import com.tech.jaxoftrades.utils.TagVIew.TagCloudLinkView;


/**
 * Created by arbaz on 22/5/17.
 */

public class TmSkillsFragment extends RootFragment implements View.OnClickListener {

    EditText etAddSkill;
    Button btnAddSkill;
    TagCloudLinkView skillTagView;
    TagSkills tagSkills;
    int tagId = 0;

    public TmSkillsFragment() {
    }

    public static TmSkillsFragment newInstance(int columnCount) {
        TmSkillsFragment fragment = new TmSkillsFragment();
        Bundle args = new Bundle();

//  args.putSerializable();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tm_skills, container, false);

        etAddSkill = (EditText) view.findViewById(R.id.etAddSkill);
        btnAddSkill = (Button) view.findViewById(R.id.btnAddSkill);
        skillTagView = (TagCloudLinkView) view.findViewById(R.id.skillTagView);
        btnAddSkill.setOnClickListener(this);
        //For click event
        skillTagView.setOnTagSelectListener(new TagCloudLinkView.OnTagSelectListener() {
            @Override
            public void onTagSelected(TagSkills tag, int i) {
//                Toast.makeText(getActivity(), tag.getText() + "=" + i, Toast.LENGTH_SHORT).show();
            }
        });
        //For Delete
        skillTagView.setOnTagDeleteListener(new TagCloudLinkView.OnTagDeleteListener() {

            @Override
            public void onTagDeleted(TagSkills tag, int i) {
//                Toast.makeText(getActivity(),"Position" + i, Toast.LENGTH_SHORT).show();
            }
        });
        return view;


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddSkill:
                String strSkill = etAddSkill.getText().toString();
                tagSkills = new TagSkills();
                tagSkills = new TagSkills(tagId++, strSkill);
                Log.e("ID==" + tagSkills.getId() + "Name==" + tagSkills.getText());
                if (!TextUtils.isEmpty(strSkill)) {
                    skillTagView.add(tagSkills);
                    skillTagView.drawTags();
                    etAddSkill.setText("");
                    //Log.e("ID==" + tagSkills.getId() + "Name==" + tagSkills.getText());
                } else {
                    etAddSkill.setError("Please enter your skill!");
                }
                break;
            default:
                break;
        }
    }
}
