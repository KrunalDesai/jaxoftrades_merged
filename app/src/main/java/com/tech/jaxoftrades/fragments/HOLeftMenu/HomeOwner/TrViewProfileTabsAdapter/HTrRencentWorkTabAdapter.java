package com.tech.jaxoftrades.fragments.HOLeftMenu.HomeOwner.TrViewProfileTabsAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel.HTrProfileTabObject;

import java.util.ArrayList;
import java.util.List;


public class HTrRencentWorkTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<HTrProfileTabObject> data = new ArrayList<>();

    public HTrRencentWorkTabAdapter(Context context, List<HTrProfileTabObject> data) {
        this.context = context;
        this.data = data;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.hr_view_tr_recent_work_row, parent, false);
        viewHolder = new HTrRencentWorkTabAdapter.MyItemHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.e("anku", "onBindViewHolder: "+data.get(position).getRwImage()[1]);
        Glide.with(context).load(data.get(position).getRwImage()[1])
                .thumbnail(0.5f)
                .override(200,200)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((HTrRencentWorkTabAdapter.MyItemHolder) holder).imgRw);


        ((HTrRencentWorkTabAdapter.MyItemHolder) holder).txtrwTitle.setText(data.get(position).getRwTitle());
        ((HTrRencentWorkTabAdapter.MyItemHolder) holder).txtrwDesc.setText(data.get(position).getRwDesc());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyItemHolder extends RecyclerView.ViewHolder {
        ImageView imgRw;
        TextView txtrwTitle,txtrwDesc;


        public MyItemHolder(View itemView) {
            super(itemView);

            imgRw = (ImageView)itemView.findViewById(R.id.imgrw);
            txtrwTitle = (TextView)itemView.findViewById(R.id.rwtitle);
            txtrwDesc = (TextView)itemView.findViewById(R.id.rwdesc);
        }

    }


}
