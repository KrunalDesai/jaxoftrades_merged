package com.tech.jaxoftrades.fragments.HOLeftMenu.HomeOwner.TrViewProfileTabsAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel.HTrProfileTabObject;

import java.util.ArrayList;
import java.util.List;


public class HTrRatingNReviewTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<HTrProfileTabObject> data = new ArrayList<>();

    public HTrRatingNReviewTabAdapter(Context context, List<HTrProfileTabObject> data) {
        this.context = context;
        this.data = data;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.adapter_htrviewprofile_rating, parent, false);
        viewHolder = new HTrRatingNReviewTabAdapter.MyItemHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

//        Glide.with(context).load(data.get(position).getUrl())
//                .thumbnail(0.5f)
//                .override(200,200)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(((HTrTabGalleryAdapter.MyItemHolder) holder).mImg);
        ((MyItemHolder) holder).txtTrName.setText(data.get(position).getRewiewHwName());
        ((MyItemHolder) holder).ratingText.setText(String.valueOf(data.get(position).getReviewRating()));
        ((MyItemHolder) holder).ratingDate.setText(data.get(position).getReviewDate());
        ((MyItemHolder) holder).reviewTitle.setText(data.get(position).getReviewTitle());
        ((MyItemHolder) holder).reviewDesc.setText(data.get(position).getReviewDesc());
        ((MyItemHolder) holder).rateTr.setRating(data.get(position).getReviewRating());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyItemHolder extends RecyclerView.ViewHolder {
        TextView txtTrName,ratingText,ratingDate,reviewTitle,reviewDesc;
        RatingBar rateTr;


        public MyItemHolder(View itemView) {
            super(itemView);

            txtTrName = (TextView)itemView.findViewById(R.id.txtTrName);
            rateTr = (RatingBar)itemView.findViewById(R.id.rateTr);
            ratingText = (TextView)itemView.findViewById(R.id.ratingText);
            ratingDate = (TextView)itemView.findViewById(R.id.ratingDate);
            reviewTitle = (TextView)itemView.findViewById(R.id.reviewTitle);
            reviewDesc = (TextView)itemView.findViewById(R.id.reviewDesc);
        }

    }


}
