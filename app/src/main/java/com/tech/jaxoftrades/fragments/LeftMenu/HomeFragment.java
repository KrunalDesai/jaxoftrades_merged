package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.NotificationDetailActivity;
import com.tech.jaxoftrades.adapter.TabsAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

/**
 * Created by arbaz on 20/5/17.
 */

public class HomeFragment extends RootFragment implements View.OnClickListener {
    ImageView tbNotification;
    private android.support.design.widget.TabLayout tabsDashboardLayout;
    private android.support.v4.view.ViewPager tabViewPager;

    TabsAdapter tabsAdapter;
    private int[] tabIcons = {
            R.drawable.tab_incoming_request_selector,
            R.drawable.tab_job_selector,
            R.drawable.tab_availability_selector
    };

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_home, container, false);
        this.tabViewPager = (ViewPager) view.findViewById(R.id.tabViewPager);
        this.tabsDashboardLayout = (TabLayout) view.findViewById(R.id.tabsDashboardLayout);


        //set Tabs ViewPager
        tabsAdapter = new TabsAdapter(getActivity(), getChildFragmentManager(), 3);
        tabViewPager.setAdapter(tabsAdapter);
        tabViewPager.setOffscreenPageLimit(1);
        tabsDashboardLayout.setupWithViewPager(tabViewPager);
        setupTabIcons();
        return view;
    }

    //Tab icons set here
    private void setupTabIcons() {
        tabsDashboardLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabsDashboardLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabsDashboardLayout.getTabAt(2).setIcon(tabIcons[2]);
    }




    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMDashBoard));
        tbNotification = displayNotification();
        tbNotification.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideNotification();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbNotification:
                Toast.makeText(getActivity(), "Notification Click", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(),NotificationDetailActivity.class));
                break;
            default:
                break;
        }
    }
}
