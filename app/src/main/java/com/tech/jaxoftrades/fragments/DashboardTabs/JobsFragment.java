package com.tech.jaxoftrades.fragments.DashboardTabs;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.Tabs.JobsAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

import java.util.ArrayList;

/**
 * Created by arbaz on 25/5/17.
 */

public class JobsFragment extends RootFragment {
    JobsAdapter jobsAdapter;
    ArrayList<String> stringArrayList;

    private android.support.v7.widget.RecyclerView rvJobs;

    public JobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_jobs, container, false);
        this.rvJobs = (RecyclerView) view.findViewById(R.id.rvJobs);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        rvJobs.setLayoutManager(mLayoutManager1);


        stringArrayList = new ArrayList<>();
        stringArrayList.add("Test Data");
        stringArrayList.add("Test Data");
        jobsAdapter = new JobsAdapter(stringArrayList, getActivity());
        rvJobs.setAdapter(jobsAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }
}