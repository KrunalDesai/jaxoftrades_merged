package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.SettingsMenus.ChangeEmailActivity;
import com.tech.jaxoftrades.activities.SettingsMenus.ChangePasswordActivity;
import com.tech.jaxoftrades.activities.SettingsMenus.NotificationActivity;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

/**
 * Created by arbaz on 20/5/17.
 */

public class SettingsFragment extends RootFragment implements View.OnClickListener {


    private android.widget.EditText TMSettingEmail;
    private android.widget.EditText TMSettingPassword;
    private android.widget.EditText TMSettingNotification;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_settings, container, false);
        this.TMSettingNotification = (EditText) view.findViewById(R.id.TMSettingNotification);
        this.TMSettingPassword = (EditText) view.findViewById(R.id.TMSettingPassword);
        this.TMSettingEmail = (EditText) view.findViewById(R.id.TMSettingEmail);

        TMSettingNotification.setOnClickListener(this);
        TMSettingPassword.setOnClickListener(this);
        TMSettingEmail.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMSettings));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.TMSettingNotification:
                startActivity(new Intent(getActivity(), NotificationActivity.class));
                break;
            case R.id.TMSettingPassword:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                break;
            case R.id.TMSettingEmail:
                startActivity(new Intent(getActivity(), ChangeEmailActivity.class));
                break;
            default:
                break;
        }

    }
}
