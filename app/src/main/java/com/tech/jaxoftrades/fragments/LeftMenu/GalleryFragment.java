package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.GalleryRecyclerViewAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.listener.GridSpacingItemDecoration;
import com.tech.jaxoftrades.model.Common.GalleryPhoto;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class GalleryFragment extends RootFragment {

    private RecyclerView recyclerView;
    private TextView emptyView;

    private GalleryRecyclerViewAdapter galleryRecyclerViewAdapter;

    private ArrayList<GalleryPhoto> arrGalleryPhotos;


    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_gallery, container, false);

// Set the adapter
        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        emptyView = (TextView) view.findViewById(R.id.emptyView);


        recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, 50, true));
        arrGalleryPhotos = new ArrayList<>();
        arrGalleryPhotos.add(new GalleryPhoto("1"));
        arrGalleryPhotos.add(new GalleryPhoto("2"));
        arrGalleryPhotos.add(new GalleryPhoto("3"));
        arrGalleryPhotos.add(new GalleryPhoto("4"));
        arrGalleryPhotos.add(new GalleryPhoto("5"));
        arrGalleryPhotos.add(new GalleryPhoto("6"));
        arrGalleryPhotos.add(new GalleryPhoto("7"));
        arrGalleryPhotos.add(new GalleryPhoto("8"));
        arrGalleryPhotos.add(new GalleryPhoto("9"));
        arrGalleryPhotos.add(new GalleryPhoto("10"));


        galleryRecyclerViewAdapter = new GalleryRecyclerViewAdapter(getActivity(), arrGalleryPhotos);
        recyclerView.setAdapter(galleryRecyclerViewAdapter);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMGallery));
    }
}
