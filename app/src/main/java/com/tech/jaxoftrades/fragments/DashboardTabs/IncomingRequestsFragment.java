package com.tech.jaxoftrades.fragments.DashboardTabs;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.Tabs.IncomingRequestAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

import java.util.ArrayList;

/**
 * Created by arbaz on 25/5/17.
 */

public class IncomingRequestsFragment extends RootFragment {

    IncomingRequestAdapter incomingRequestAdapter;
    ArrayList<String> stringArrayList;
    private android.support.v7.widget.RecyclerView rvIncomingReq;
    private android.widget.LinearLayout llInComingReqEmpty;


    public IncomingRequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_incoming_requests, container, false);
        this.llInComingReqEmpty = (LinearLayout) view.findViewById(R.id.llInComingReqEmpty);

        this.rvIncomingReq = (RecyclerView) view.findViewById(R.id.rvIncomingReq);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        rvIncomingReq.setLayoutManager(mLayoutManager1);



        stringArrayList = new ArrayList<>();
        stringArrayList.add("Test Data");


        if (!stringArrayList.isEmpty()) {
            incomingRequestAdapter = new IncomingRequestAdapter(stringArrayList, getActivity());
            rvIncomingReq.setAdapter(incomingRequestAdapter);
        } else {
            llInComingReqEmpty.setVisibility(View.VISIBLE);
            rvIncomingReq.setVisibility(View.GONE);
        }

        if(incomingRequestAdapter != null){
            incomingRequestAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    llInComingReqEmpty.setVisibility(View.VISIBLE);
                    rvIncomingReq.setVisibility(View.GONE);
                }
            });
        }

        llInComingReqEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llInComingReqEmpty.setVisibility(View.GONE);
                rvIncomingReq.setVisibility(View.VISIBLE);
                stringArrayList.add("Test Data");
                incomingRequestAdapter = new IncomingRequestAdapter(stringArrayList, getActivity());
                incomingRequestAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        llInComingReqEmpty.setVisibility(View.VISIBLE);
                        rvIncomingReq.setVisibility(View.GONE);
                    }
                });
                rvIncomingReq.setAdapter(incomingRequestAdapter);

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

}
