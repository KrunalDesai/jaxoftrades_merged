package com.tech.jaxoftrades.fragments.HOFragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tech.jaxoftrades.R;


public class HomeRequestDailogeFragment extends DialogFragment {

    private TextView mtxtText;
    private Button btnCancel;

    public HomeRequestDailogeFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static HomeRequestDailogeFragment newInstance(String title) {
        HomeRequestDailogeFragment frag = new HomeRequestDailogeFragment();

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogue_fragment_homerequest, container, true);
        getDialog().setCancelable(false);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        mtxtText = (TextView) view.findViewById(R.id.txtTimeRemaining);
        mtxtText.setTypeface(null, Typeface.BOLD);
        btnCancel = (Button) view.findViewById(R.id.buttonCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
 //       getDialog().setCancelable(true);
        // Fetch arguments from bundle and set title
//        String title = getArguments().getString("title", "Enter Name");
//        getDialog().setTitle(title);


        new CountDownTimer(121000, 1000) {

            public void onTick(long millisUntilFinished) {
                mtxtText.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                mtxtText.setText("done!");
            }
        }.start();

    }

}
