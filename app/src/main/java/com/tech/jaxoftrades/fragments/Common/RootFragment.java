package com.tech.jaxoftrades.fragments.Common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.listener.OnBackPressListener;

/**
 * Created by arbaz on 20/5/17.
 */

public class RootFragment extends Fragment implements OnBackPressListener {
    private Toolbar toolbar;

    public TextView tbTitle;
    public TextView tbSave;

    private ImageView tbBack;
    private ImageView tbNotification;
    private ImageView tbNotificationLive;

    public boolean isBackClick = true;


    public OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {


            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

            tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
            tbSave = (TextView) toolbar.findViewById(R.id.tbSave);

            tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
            tbNotification = (ImageView) toolbar.findViewById(R.id.tbNotification);
            tbNotificationLive = (ImageView) toolbar.findViewById(R.id.tbNotificationLive);


        }

    }


    public void replaceFragment(int containerId, Fragment fragment, String title) {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.addToBackStack(title);
        transaction.replace(containerId, fragment, title).commit();
    }

    public void setTitle(String title) {

        if (tbTitle != null)
            tbTitle.setText(title);
    }


    /*Related To back ImageView*/
    public void displayBack() {
        tbBack.setVisibility(View.VISIBLE);
        if (isBackClick) {
            tbBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        if (mListener != null)
                            mListener.onBackClicked();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void hideBack() {
        tbBack.setVisibility(View.GONE);
    }

    /*Related To Save TextView*/
    public TextView displaySave() {
        tbSave.setVisibility(View.VISIBLE);
        return tbSave;
    }

    public void hideSave() {
        tbSave.setVisibility(View.GONE);

    }

    /*Related To Notification ImageVIew*/
    public ImageView displayNotification() {
        tbNotification.setVisibility(View.VISIBLE);
        return tbNotification;
    }

    public void hideNotification() {
        tbNotification.setVisibility(View.GONE);

    }

    /*Related To NotificationLive ImageVIew*/
    public ImageView displayNotificationLive() {
        tbNotificationLive.setVisibility(View.VISIBLE);
        return tbNotificationLive;
    }

    public void hideNotificationLive() {
        tbNotificationLive.setVisibility(View.GONE);

    }

    public void oneBackStack() {

        for (int i = 0; i < 1; i++) {
            tbBack.performClick();
        }
    }


    public void clearBackStack() {

        for (int i = 0; i < 2; i++) {
            tbBack.performClick();
        }


    }

    @Override
    public boolean onBackPressed() {
        return false;
    }


    public interface OnFragmentInteractionListener {
        void onBackClicked();
    }

}

