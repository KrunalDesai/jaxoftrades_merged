package com.tech.jaxoftrades.fragments.TradesManProfile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

/**
 * Created by arbaz on 22/5/17.
 */

public class TmAboutYouFragment extends RootFragment {


    public TmAboutYouFragment() {
    }

    public static TmAboutYouFragment newInstance(int columnCount) {
        TmAboutYouFragment fragment = new TmAboutYouFragment();
        Bundle args = new Bundle();

//  args.putSerializable();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tm_about_you, container, false);


        return view;
    }


}
