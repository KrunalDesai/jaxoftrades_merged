package com.tech.jaxoftrades.fragments.HOLeftMenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.HomeOwner.HOHistoryAdapter;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeownerHistory;

import java.util.ArrayList;
import java.util.List;


public class HOHistoryFragment extends Fragment {



    private List<HomeownerHistory> movieList;
    private RecyclerView recyclerView;
    private HOHistoryAdapter mAdapter;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_homeowner_history, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rvHomeHistory);


        movieList = new ArrayList<>();
        mAdapter = new HOHistoryAdapter(getActivity(),movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();


        

        return rootView;
    }

    private void prepareMovieData() {

        HomeownerHistory movie = new HomeownerHistory("David Smith", "Plumbing works on 23rd Sep 2016", "Status : In progress");
        movieList.add(movie);

        movie = new HomeownerHistory("Sarah Hill", "Painting works on 19th sep 2016", "Status : Completed");
        movieList.add(movie);

        movie = new HomeownerHistory("David Smith", "Plumbing works on 23rd Sep 2016", "Status : In progress");
        movieList.add(movie);

        movie = new HomeownerHistory("Thomas Brown","Electrician works on 5th Sep 2016","Status : Completed");
        movieList.add(movie);

        movie = new HomeownerHistory("James Fisher", "Plumbing works on 1st Sep 2016", "Status : Completed");
        movieList.add(movie);


        mAdapter.notifyDataSetChanged();

    }
}
