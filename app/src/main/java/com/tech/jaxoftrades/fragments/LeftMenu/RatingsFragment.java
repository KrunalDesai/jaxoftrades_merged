package com.tech.jaxoftrades.fragments.LeftMenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.Common.RootFragment;

/**
 * Created by arbaz on 20/5/17.
 */

public class RatingsFragment extends RootFragment implements View.OnClickListener {

    ImageView tbNotificationLive;

    public RatingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_ratings, container, false);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMRatings));
        tbNotificationLive = displayNotificationLive();
        tbNotificationLive.setOnClickListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        hideNotificationLive();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbNotificationLive:
                Toast.makeText(getActivity(), "NotificationLive Click", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

}
