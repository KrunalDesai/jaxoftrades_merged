package com.tech.jaxoftrades.fragments.LeftMenu;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.Tabs.JobsAdapter;
import com.tech.jaxoftrades.fragments.Common.RootFragment;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class HistoryFragment extends RootFragment implements OnApiCallListener {
    JobsAdapter jobsAdapter;
    ArrayList<String> stringArrayList;
    RecyclerView rvHistory;
    TextView tvHistoryEmpty;


    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiFunctions = new ApiFunctions(getActivity(), this);
        //getting data
        strUserResponse = Global.getPreference("UserResponseData", "");
        userResponse = gson.fromJson(strUserResponse.toString(), UserResponse.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_history, container, false);
        rvHistory = (RecyclerView) view.findViewById(R.id.rvHistory);
        tvHistoryEmpty = (TextView) view.findViewById(R.id.tvHistoryEmpty);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(mLayoutManager1);
        try {
            if (Global.isNetworkAvailable(getActivity())) {
                AppDialog.showProgressDialog(getActivity());
                apiFunctions.getHistoryList(Api.MainUrl + Api.ActionTmHistory);

            } else {
                AppDialog.noNetworkDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.TMLMHistory));
    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        AppDialog.dismissProgressDialog();
        Log.e("History List Success" + responseString);
        JSONObject jsonObject;
        stringArrayList = new ArrayList<>();
        String tempData;

        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                    JSONArray jsonArray = jsonObject.getJSONArray("History_data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        tempData = jsonArray.get(i).toString();
                        stringArrayList.add(tempData);

                    }
                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {

                    //display error message as per response

                } else {
                    //when response code not match default message will display
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {

                            if (!stringArrayList.isEmpty() || stringArrayList.size() < 0) {
                                rvHistory.setVisibility(View.VISIBLE);
                                tvHistoryEmpty.setVisibility(View.GONE);
                                jobsAdapter = new JobsAdapter(stringArrayList, getActivity());
                                rvHistory.setAdapter(jobsAdapter);
                            } else {
                                rvHistory.setVisibility(View.GONE);
                                tvHistoryEmpty.setVisibility(View.VISIBLE);
                            }

                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(getActivity(), null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(getActivity(), null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        AppDialog.dismissProgressDialog();
        Log.e("History List  Failure" + errorMessage);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(getActivity(), null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

    }
}
