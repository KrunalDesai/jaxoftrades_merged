package com.tech.jaxoftrades;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.tech.jaxoftrades.model.Common.DeviceInfo;

/**
 * Created by arbaz on 19/5/17.
 */

public class JaxOfTradesApplication extends Application {
    public static DeviceInfo deviceInfo;
    public static SharedPreferences sharedPref;

    @Override
    public void onCreate() {
        super.onCreate();


        //fresco Image viewer
        Fresco.initialize(this);
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearMemoryCaches();
        imagePipeline.clearDiskCaches();

        deviceInfo = new DeviceInfo(this);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }
}
