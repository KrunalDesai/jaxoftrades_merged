package com.tech.jaxoftrades.model.HomOwnerModel;

import java.io.Serializable;


public class HomeOwnerLeftMenuMain implements Serializable{
    public String strHOMenuTitle;
    public int imgId;

    public HomeOwnerLeftMenuMain(String strHOMenuTitle) {
        this.strHOMenuTitle = strHOMenuTitle;
    }



    public String getStrHOMenuTitle() {
        return strHOMenuTitle;
    }

    public void setStrHOMenuTitle(String strHOMenuTitle) {
        this.strHOMenuTitle = strHOMenuTitle;
    }


}
