package com.tech.jaxoftrades.model.Common;

import java.io.Serializable;

/**
 * Created by arbaz on 6/6/17.
 */

public class CompanyProfileListMain implements Serializable {
    public String itemName;
    public int itemIcon;

    public CompanyProfileListMain(String itemName ) {
        this.itemName = itemName;

    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon(int itemIcon) {
        this.itemIcon = itemIcon;
    }
}
