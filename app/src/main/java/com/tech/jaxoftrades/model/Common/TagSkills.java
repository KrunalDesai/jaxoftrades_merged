package com.tech.jaxoftrades.model.Common;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by arbaz on 20/6/17.
 */

public class TagSkills implements Serializable {
    private int id;
    private String text;
    private Map<?,?> attrs;

    public TagSkills() {
    }

    public TagSkills(int id, String text){
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<?, ?> getAttrs() {
        return attrs;
    }

    public void setAttrs(Map<?, ?> attrs) {
        this.attrs = attrs;
    }
}
