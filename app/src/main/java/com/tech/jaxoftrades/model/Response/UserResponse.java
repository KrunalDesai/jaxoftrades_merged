package com.tech.jaxoftrades.model.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserResponse implements Serializable{

	@SerializedName("DATA")
	private List<UserDetailsMain> dATA;

	@SerializedName("History_data")
	private List<Object> historyData;

	@SerializedName("incoming_requests")
	private List<Object> incomingRequests;

	public void setDATA(List<UserDetailsMain> dATA){
		this.dATA = dATA;
	}

	public List<UserDetailsMain> getDATA(){
		return dATA;
	}

	public void setHistoryData(List<Object> historyData){
		this.historyData = historyData;
	}

	public List<Object> getHistoryData(){
		return historyData;
	}

	public void setIncomingRequests(List<Object> incomingRequests){
		this.incomingRequests = incomingRequests;
	}

	public List<Object> getIncomingRequests(){
		return incomingRequests;
	}


}