package com.tech.jaxoftrades.model.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

 public class CategoryItem{

	@SerializedName("job_type")
	private String jobType;

	@SerializedName("recentwork")
	private List<Object> recentwork;

	@SerializedName("website")
	private String website;

	@SerializedName("start_am_pm")
	private String startAmPm;

	@SerializedName("twitter_link")
	private String twitterLink;

	@SerializedName("about_you")
	private String aboutYou;

	@SerializedName("Default_location")
	private String defaultLocation;

	@SerializedName("facebook_link")
	private String facebookLink;

	@SerializedName("skills")
	private String skills;

	@SerializedName("daily_time")
	private String dailyTime;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("start_hour")
	private String startHour;

	@SerializedName("linked_in")
	private String linkedIn;

	@SerializedName("end_minute")
	private String endMinute;

	@SerializedName("end_am_pm")
	private String endAmPm;

	@SerializedName("dnd")
	private String dnd;

	@SerializedName("end_hour")
	private String endHour;

	@SerializedName("job_sub_type")
	private List<String> jobSubType;

	@SerializedName("predefined_skills")
	private String predefinedSkills;

	@SerializedName("start_minute")
	private String startMinute;

	@SerializedName("gallery")
	private List<Object> gallery;

	public void setJobType(String jobType){
		this.jobType = jobType;
	}

	public String getJobType(){
		return jobType;
	}

	public void setRecentwork(List<Object> recentwork){
		this.recentwork = recentwork;
	}

	public List<Object> getRecentwork(){
		return recentwork;
	}

	public void setWebsite(String website){
		this.website = website;
	}

	public String getWebsite(){
		return website;
	}

	public void setStartAmPm(String startAmPm){
		this.startAmPm = startAmPm;
	}

	public String getStartAmPm(){
		return startAmPm;
	}

	public void setTwitterLink(String twitterLink){
		this.twitterLink = twitterLink;
	}

	public String getTwitterLink(){
		return twitterLink;
	}

	public void setAboutYou(String aboutYou){
		this.aboutYou = aboutYou;
	}

	public String getAboutYou(){
		return aboutYou;
	}

	public void setDefaultLocation(String defaultLocation){
		this.defaultLocation = defaultLocation;
	}

	public String getDefaultLocation(){
		return defaultLocation;
	}

	public void setFacebookLink(String facebookLink){
		this.facebookLink = facebookLink;
	}

	public String getFacebookLink(){
		return facebookLink;
	}

	public void setSkills(String skills){
		this.skills = skills;
	}

	public String getSkills(){
		return skills;
	}

	public void setDailyTime(String dailyTime){
		this.dailyTime = dailyTime;
	}

	public String getDailyTime(){
		return dailyTime;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setStartHour(String startHour){
		this.startHour = startHour;
	}

	public String getStartHour(){
		return startHour;
	}

	public void setLinkedIn(String linkedIn){
		this.linkedIn = linkedIn;
	}

	public String getLinkedIn(){
		return linkedIn;
	}

	public void setEndMinute(String endMinute){
		this.endMinute = endMinute;
	}

	public String getEndMinute(){
		return endMinute;
	}

	public void setEndAmPm(String endAmPm){
		this.endAmPm = endAmPm;
	}

	public String getEndAmPm(){
		return endAmPm;
	}

	public void setDnd(String dnd){
		this.dnd = dnd;
	}

	public String getDnd(){
		return dnd;
	}

	public void setEndHour(String endHour){
		this.endHour = endHour;
	}

	public String getEndHour(){
		return endHour;
	}

	public void setJobSubType(List<String> jobSubType){
		this.jobSubType = jobSubType;
	}

	public List<String> getJobSubType(){
		return jobSubType;
	}

	public void setPredefinedSkills(String predefinedSkills){
		this.predefinedSkills = predefinedSkills;
	}

	public String getPredefinedSkills(){
		return predefinedSkills;
	}

	public void setStartMinute(String startMinute){
		this.startMinute = startMinute;
	}

	public String getStartMinute(){
		return startMinute;
	}

	public void setGallery(List<Object> gallery){
		this.gallery = gallery;
	}

	public List<Object> getGallery(){
		return gallery;
	}

	@Override
 	public String toString(){
		return 
			"CategoryItem{" + 
			"job_type = '" + jobType + '\'' + 
			",recentwork = '" + recentwork + '\'' + 
			",website = '" + website + '\'' + 
			",start_am_pm = '" + startAmPm + '\'' + 
			",twitter_link = '" + twitterLink + '\'' + 
			",about_you = '" + aboutYou + '\'' + 
			",default_location = '" + defaultLocation + '\'' + 
			",facebook_link = '" + facebookLink + '\'' + 
			",skills = '" + skills + '\'' + 
			",daily_time = '" + dailyTime + '\'' + 
			",company_name = '" + companyName + '\'' + 
			",start_hour = '" + startHour + '\'' + 
			",linked_in = '" + linkedIn + '\'' + 
			",end_minute = '" + endMinute + '\'' + 
			",end_am_pm = '" + endAmPm + '\'' + 
			",dnd = '" + dnd + '\'' + 
			",end_hour = '" + endHour + '\'' + 
			",job_sub_type = '" + jobSubType + '\'' + 
			",predefined_skills = '" + predefinedSkills + '\'' + 
			",start_minute = '" + startMinute + '\'' + 
			",gallery = '" + gallery + '\'' + 
			"}";
		}
}