package com.tech.jaxoftrades.model.Common;

import java.io.Serializable;

/**
 * Created by ashish on 16/5/17.
 */

public class GalleryPhoto implements Serializable {

    transient public String key;
    public String photoTitle;
    public String photoUrl;
    public String photoDescription;
    public long timestamp;

    public GalleryPhoto() {
    }

    public GalleryPhoto(String photoTitle) {
        this.photoTitle = photoTitle;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoDescription() {
        return photoDescription;
    }

    public void setPhotoDescription(String photoDescription) {
        this.photoDescription = photoDescription;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GalleryPhoto)) {
            return false;
        }

        GalleryPhoto galleryPhoto = (GalleryPhoto) obj;
        return key.equals(galleryPhoto.key);
    }
}
