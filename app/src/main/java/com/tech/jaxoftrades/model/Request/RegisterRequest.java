package com.tech.jaxoftrades.model.Request;

import java.io.Serializable;

public class RegisterRequest implements Serializable {
	public String email;
	public String hFirstName;
	public String hLastName;
	public String password;
	public String contactNumber;
	public String companyName;
	public String type;

	public RegisterRequest(String email, String hFirstName, String hLastName, String password, String contactNumber, String companyName, String type) {
		this.email = email;
		this.hFirstName = hFirstName;
		this.hLastName = hLastName;
		this.password = password;
		this.contactNumber = contactNumber;
		this.companyName = companyName;
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String gethFirstName() {
		return hFirstName;
	}

	public void sethFirstName(String hFirstName) {
		this.hFirstName = hFirstName;
	}

	public String gethLastName() {
		return hLastName;
	}

	public void sethLastName(String hLastName) {
		this.hLastName = hLastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}