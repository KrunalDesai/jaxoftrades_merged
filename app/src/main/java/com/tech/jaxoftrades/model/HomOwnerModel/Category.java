package com.tech.jaxoftrades.model.HomOwnerModel;

/**
 * Created by Krunal on 2/27/2017.
 */

public class Category {

    private String categoryName;
    private int thumbnail;

    public Category(){

    }

    public Category(String categoryName, int thumbnail) {
        this.categoryName = categoryName;
        this.thumbnail = thumbnail;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String name) {
        this.categoryName = categoryName;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

}
