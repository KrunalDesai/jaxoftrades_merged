package com.tech.jaxoftrades.model.Common;

import java.io.Serializable;

/**
 * Created by arbaz on 9/6/17.
 */

public class ApiResponse implements Serializable {
    public String STATUS;
    public String ERR_MSG;
    public String Message="";

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getERR_MSG() {
        return ERR_MSG;
    }

    public void setERR_MSG(String ERR_MSG) {
        this.ERR_MSG = ERR_MSG;
    }


    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
