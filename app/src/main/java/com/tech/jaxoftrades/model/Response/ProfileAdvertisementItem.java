package com.tech.jaxoftrades.model.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileAdvertisementItem implements Serializable{

	@SerializedName("job_type")
	private String jobType;

	@SerializedName("daily_time")
	private String dailyTime;

	@SerializedName("dnd")
	private String dnd;

	public void setJobType(String jobType){
		this.jobType = jobType;
	}

	public String getJobType(){
		return jobType;
	}

	public void setDailyTime(String dailyTime){
		this.dailyTime = dailyTime;
	}

	public String getDailyTime(){
		return dailyTime;
	}

	public void setDnd(String dnd){
		this.dnd = dnd;
	}

	public String getDnd(){
		return dnd;
	}

	@Override
 	public String toString(){
		return 
			"ProfileAdvertisementItem{" +
			"job_type = '" + jobType + '\'' + 
			",daily_time = '" + dailyTime + '\'' + 
			",dnd = '" + dnd + '\'' + 
			"}";
		}
}