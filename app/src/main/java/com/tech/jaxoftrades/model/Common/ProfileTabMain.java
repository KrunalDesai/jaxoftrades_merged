package com.tech.jaxoftrades.model.Common;

import android.support.v4.app.Fragment;

import java.io.Serializable;

/**
 * Created by arbaz on 19/6/17.
 */

public class ProfileTabMain implements Serializable {
    public String title;
    public int catID;
    public Fragment fragment;

    public ProfileTabMain(String title) {
        this.title = title;
    }

    public ProfileTabMain(String title, int catID) {
        this.title = title;
        this.catID = catID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCatID() {
        return catID;
    }

    public void setCatID(int catID) {
        this.catID = catID;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
