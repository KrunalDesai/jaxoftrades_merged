package com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel;

import android.os.Parcel;
import android.os.Parcelable;


public class HViewProfileGalleryObject implements Parcelable {

    String name, url;

    public HViewProfileGalleryObject() {

    }

    protected HViewProfileGalleryObject(Parcel in) {
        name = in.readString();
        url = in.readString();
    }

    public static final Creator<HViewProfileGalleryObject> CREATOR = new Creator<HViewProfileGalleryObject>() {
        @Override
        public HViewProfileGalleryObject createFromParcel(Parcel in) {
            return new HViewProfileGalleryObject(in);
        }

        @Override
        public HViewProfileGalleryObject[] newArray(int size) {
            return new HViewProfileGalleryObject[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
    }
}
