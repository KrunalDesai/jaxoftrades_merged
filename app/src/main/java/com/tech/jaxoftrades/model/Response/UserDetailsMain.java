package com.tech.jaxoftrades.model.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserDetailsMain implements Serializable{

	@SerializedName("job_type")
	private String jobType;

	@SerializedName("login_id")
	private String loginId;

	@SerializedName("tr_last_name")
	private String trLastName;

	@SerializedName("contact_number")
	private String contactNumber;

	@SerializedName("profile_imgpath")
	private String profileImgpath;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("total_rated")
	private String totalRated;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("avg_rating")
	private String avgRating;

	@SerializedName("company")
	private List<Object> company;

	@SerializedName("tr_first_name")
	private String trFirstName;

	@SerializedName("t_text")
	private String tText;

	@SerializedName("category")
	private List<CategoryItem> category;

	@SerializedName("job_sub_type")
	private List<String> jobSubType;

	@SerializedName("email")
	private String email;

	@SerializedName("tradesmen_id")
	private String tradesmenId;

	public void setJobType(String jobType){
		this.jobType = jobType;
	}

	public String getJobType(){
		return jobType;
	}

	public void setLoginId(String loginId){
		this.loginId = loginId;
	}

	public String getLoginId(){
		return loginId;
	}

	public void setTrLastName(String trLastName){
		this.trLastName = trLastName;
	}

	public String getTrLastName(){
		return trLastName;
	}

	public void setContactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}

	public String getContactNumber(){
		return contactNumber;
	}

	public void setProfileImgpath(String profileImgpath){
		this.profileImgpath = profileImgpath;
	}

	public String getProfileImgpath(){
		return profileImgpath;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setTotalRated(String totalRated){
		this.totalRated = totalRated;
	}

	public String getTotalRated(){
		return totalRated;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setAvgRating(String avgRating){
		this.avgRating = avgRating;
	}

	public String getAvgRating(){
		return avgRating;
	}

	public void setCompany(List<Object> company){
		this.company = company;
	}

	public List<Object> getCompany(){
		return company;
	}

	public void setTrFirstName(String trFirstName){
		this.trFirstName = trFirstName;
	}

	public String getTrFirstName(){
		return trFirstName;
	}

	public void setTText(String tText){
		this.tText = tText;
	}

	public String getTText(){
		return tText;
	}

	public void setCategory(List<CategoryItem> category){
		this.category = category;
	}

	public List<CategoryItem> getCategory(){
		return category;
	}

	public void setJobSubType(List<String> jobSubType){
		this.jobSubType = jobSubType;
	}

	public List<String> getJobSubType(){
		return jobSubType;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setTradesmenId(String tradesmenId){
		this.tradesmenId = tradesmenId;
	}

	public String getTradesmenId(){
		return tradesmenId;
	}


}