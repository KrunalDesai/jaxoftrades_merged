package com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel;

import java.io.Serializable;


public class HTrProfileTabObject implements Serializable {

    int tId;
    String tName,tAbout,tSpecialization,availableTime;
    Float tDistance,tRatings;
    int rid;
    Float reviewRating;
    String rwTitle,rwDesc,galleryImage,reviewTitle,reviewDesc,reviewDate,rewiewHwName;
    String[] rwImage;

    public HTrProfileTabObject() {
        tId = 0;
        tName = "";
        tAbout= "";
        tSpecialization = "";
        availableTime = "";
        tDistance = 0.0f;
        tRatings = 0.0f;
        rid = 0;
        reviewRating = 0.0f;
        rwTitle = "";
        rwDesc = "";
        galleryImage = "";
        reviewTitle = "";
        reviewDesc = "";
        reviewDate = "";
        rewiewHwName = "";
    }

    public HTrProfileTabObject(String rewiewHwName, Float reviewRating, String reviewTitle, String reviewDesc, String reviewDate) {
        this.reviewRating = reviewRating;
        this.reviewTitle = reviewTitle;
        this.reviewDesc = reviewDesc;
        this.reviewDate = reviewDate;
        this.rewiewHwName = rewiewHwName;
    }


    public HTrProfileTabObject(String rwTitle, String rwDesc, String[] rwImage) {
        this.rwTitle = rwTitle;
        this.rwDesc = rwDesc;
        this.rwImage = rwImage;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public String gettAbout() {
        return tAbout;
    }

    public void settAbout(String tAbout) {
        this.tAbout = tAbout;
    }

    public String gettSpecialization() {
        return tSpecialization;
    }

    public void settSpecialization(String tSpecialization) {
        this.tSpecialization = tSpecialization;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public Float gettDistance() {
        return tDistance;
    }

    public void settDistance(Float tDistance) {
        this.tDistance = tDistance;
    }

    public Float gettRatings() {
        return tRatings;
    }

    public void settRatings(Float tRatings) {
        this.tRatings = tRatings;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public Float getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(Float reviewRating) {
        this.reviewRating = reviewRating;
    }

    public String[] getRwImage() {
        return rwImage;
    }

    public void setRwImage(String[] rwImage) {
        this.rwImage = rwImage;
    }

    public String getRwTitle() {
        return rwTitle;
    }

    public void setRwTitle(String rwTitle) {
        this.rwTitle = rwTitle;
    }

    public String getRwDesc() {
        return rwDesc;
    }

    public void setRwDesc(String rwDesc) {
        this.rwDesc = rwDesc;
    }

    public String getGalleryImage() {
        return galleryImage;
    }

    public void setGalleryImage(String galleryImage) {
        this.galleryImage = galleryImage;
    }

    public String getReviewTitle() {
        return reviewTitle;
    }

    public void setReviewTitle(String reviewTitle) {
        this.reviewTitle = reviewTitle;
    }

    public String getReviewDesc() {
        return reviewDesc;
    }

    public void setReviewDesc(String reviewDesc) {
        this.reviewDesc = reviewDesc;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getRewiewHwName() {
        return rewiewHwName;
    }

    public void setRewiewHwName(String rewiewHwName) {
        this.rewiewHwName = rewiewHwName;
    }
}
