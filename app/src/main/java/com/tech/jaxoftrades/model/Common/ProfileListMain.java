package com.tech.jaxoftrades.model.Common;

import java.io.Serializable;

/**
 * Created by arbaz on 6/6/17.
 */

public class ProfileListMain implements Serializable {
    public String itemName;
    public int itemIcon;

    public ProfileListMain(String itemName, int itemIcon) {
        this.itemName = itemName;
        this.itemIcon = itemIcon;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon(int itemIcon) {
        this.itemIcon = itemIcon;
    }
}
