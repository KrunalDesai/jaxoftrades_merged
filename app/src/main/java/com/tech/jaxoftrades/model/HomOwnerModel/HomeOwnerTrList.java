package com.tech.jaxoftrades.model.HomOwnerModel;

/**
 * Created by Krunal on 2/27/2017.
 */

public class HomeOwnerTrList {

    private String name, category, companyname,timeAvail,distance,listAvgRating;

    public HomeOwnerTrList(){

    }

    public HomeOwnerTrList(String name, String category, String companyname, String timeAvail, String distance, String listAvgRating) {

        this.name = name;
        this.category = category;
        this.companyname = companyname;
        this.timeAvail = timeAvail;
        this.distance = distance;
        this.listAvgRating =listAvgRating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyName(String companyname) {
        this.companyname = companyname;
    }

    public String getTimeAvail() {
        return timeAvail;
    }

    public void setTimeAvail(String timeAvail) {
        this.timeAvail = timeAvail;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getListAvgRating() {
        return listAvgRating;
    }

    public void setListAvgRating(String listAvgRating) {
        this.listAvgRating = listAvgRating;
    }
}
