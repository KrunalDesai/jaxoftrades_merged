package com.tech.jaxoftrades.webServices;

/**
 * Created by arbaz on 9/6/17.
 */

public class Api {

    // Header
    public static final String HTTP_HEADER = "httpx-thetatech-accesstoken";

    //Dump Condition
    public static final int ConnectionTimeout = 30000; // = 30 seconds
    public static final int ConnectionSoTimeout = 60000; // = 60 seconds

    //JaxOfTrades Codes
    public static final int ResponseOk = 200;
    public static final int ResponseCreated = 201;
    public static final int ResponsePageError = 400;
    public static final int ResponseUnauthorized = 401;
    public static final int ResponseForgotEmail = 422;
    public static final int ResponseServerError = 500;

    //JaxOfTrades String Codes
    public static final String StrResponseOk = "OK";
    public static final String StrResponseNotOk = "NOT_OK";
    public static final String StrResponseError = "ERR";

    public static final String ServerErrorMessage = "We are unable to connect with server, please try again later";

    //JaxOfTrades  Params
    public static final String data = "DATA";
    public static final String code = "code";
    public static String message = "message";
    public static final String error = "error";
    public static final String description = "description";
    public static final String validateKeys = "validate_keys";
    public static final String validateDescription = "validate_description";

    //List Params
    public static final int ListLimit = 10;
    public static final int ApprovedStatus = 4;
    public static final int PendingStatus = 0;


    //Live Url
    public static final String MainUrl = "http://www.jaxoftrades.com/api/";

    //Local Url
    //public static final String MainUrl = "www.jaxoftrades.com/api/";

    //Api End Points TradesMan Side
    public static final String ActionLogin = "login.php";
    public static final String ActionRegister = "signup.php";
    public static final String ActionTmTestimonial = "testimonials.php";
    public static final String ActionTmForgotPassword= "mforgotpswd.php";
    public static final String ActionTmChangePassword= "settings_changepassword.php";


    public static final String ActionTmHistory= "history.php";


}
