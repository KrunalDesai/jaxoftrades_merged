package com.tech.jaxoftrades.webServices;

import android.content.Context;

import com.google.gson.Gson;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by arbaz on 9/6/17.
 */

public class ApiFunctions implements Serializable {

    private final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    Builder b = new Builder();
    private OkHttpClient client;
    private Context context;
    private OnApiCallListener acListener;
    private Gson gson;
    private MediaType CONTENT_TYPE = MediaType.parse("multipart/form-data; charset=utf-8");


    /*Constructor with one parameter*/
    public ApiFunctions(Context context) {

        this.client = new OkHttpClient();
        this.context = context;
        this.acListener = (OnApiCallListener) context;
        this.gson = new Gson();
        try {
            b.connectTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
            b.writeTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
            b.readTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
        } catch (Exception e) {
        }

        File cacheDirectory = new File(context.getCacheDir(), "http");
        int cacheSize = 10 * 1024 * 1024;
        try {
            Cache cache = new Cache(cacheDirectory, cacheSize);
            b.cache(cache);
            client = b.build();
        } catch (Exception e) {
            Log.v("Exception " + e.getMessage());
        }

    }


    public ApiFunctions(Context context, OnApiCallListener acListener) {

        this.client = new OkHttpClient();
        this.context = context;
        this.acListener = acListener;
        this.gson = new Gson();


        try {
            b.connectTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
            b.writeTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
            b.readTimeout(Api.ConnectionTimeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            Log.v("Exception " + e.getMessage());
        }

        File cacheDirectory = new File(context.getCacheDir(), "http");
        int cacheSize = 10 * 1024 * 1024;
        try {
            Cache cache = new Cache(cacheDirectory, cacheSize);
            b.cache(cache);
            client = b.build();
        } catch (Exception e) {
            Log.v("Exception " + e.getMessage());
        }
    }



    public void executeRequest(final String url, Request request) {

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                acListener.onFailure(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int responseCode = response.code();
                String responseString = response.body().string();

                acListener.onSuccess(responseCode, responseString, url);

            }


        });

    }

    // API Function For Login
    public void userLogin(String url, String email, String password) {
        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("email", email)
                    .add("password", password)
                    .build();
            Request request = new Request.Builder().url(url).post(formBody).build();
            executeRequest(url, request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // API Function For Register
    public void userRegister(String url, String type, String email, String tr_first_name, String tr_last_name, String password, String contact_number) {
        try {

            RequestBody formBody = new FormBody.Builder()
                    .add("type", type)
                    .add("email", email)
                    .add("tr_first_name", tr_first_name)
                    .add("tr_last_name", tr_last_name)
                    .add("password", password)
                    .add("contact_number", contact_number)
                    .build();
            Request request = new Request.Builder().url(url).post(formBody).build();
            executeRequest(url, request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // API Function For Forgot Password
    public void forgotPassword(String url, String email) {
        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("email", email)
                    .build();
            Request request = new Request.Builder().url(url).post(formBody).build();
            executeRequest(url, request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // API Function For Get History List(From Left Menu)
    public void getHistoryList(String url) {
        try {
            Request request = new Request.Builder().url(url).get().build();
            executeRequest(url, request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // API Function For Tradesman Testimonial
    public void tmTestimonial(String url, String type, String tradesmen_id, String t_text) {
        try {
            MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(CONTENT_TYPE);
            RequestBody postData = multipartBody
                    .addFormDataPart("type", type)
                    .addFormDataPart("tradesmen_id", tradesmen_id)
                    .addFormDataPart("t_text", t_text).build();
            Request request = new Request.Builder().url(url).post(postData).build();
            executeRequest(url, request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
