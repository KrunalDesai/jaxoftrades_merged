package com.tech.jaxoftrades.CustomDialogs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.tech.jaxoftrades.R;


public class HoSubcategorySelection extends DialogFragment implements View.OnClickListener {


    Button btnNext;
    LinearLayout layout;
    View rootView;
    String cat="";
    HomeServiceTimeFragment hSDialog;
    public HoSubcategorySelection() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }
    public static HoSubcategorySelection newInstance(String title) {
        HoSubcategorySelection frag = new HoSubcategorySelection();

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_hosubcategory_selection, container, true);
        getDialog().setCancelable(false);
        cat = getArguments().getString("Category");

        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        init();


//        btnCancel = (Button) view.findViewById(R.id.buttonCancel);
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getDialog().dismiss();
//            }
//        });
        //       getDialog().setCancelable(true);
        // Fetch arguments from bundle and set title
//        String title = getArguments().getString("title", "Enter Name");
//        getDialog().setTitle(title);


    }

    private void init() {

        btnNext= (Button) rootView.findViewById(R.id.btnNext);
        layout= (LinearLayout) rootView.findViewById(R.id.linlay);
        btnNext.setOnClickListener(this);

        if(cat == "PAINTER") {
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Interior Painter");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Exterior Painter");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);
        }else if(cat == "PLUMBER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Drain Services");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Sprinkler Systems");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Water Filtration & Purification");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Septic Tank Cleaning,Repair & Installation");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);
        }else if(cat == "ELECTRICIAN"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Alarm Systems");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Lighting");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Appliance Repair");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Installing/Electrical Wiring");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);
        }else if(cat == "GARDNER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Fences & Gates Decks");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Lawn Maintenance");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Sprinkler Systems");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Swimming Pools, Spas & Hot Tubs Gazebos & Sheds");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);
        }else if(cat == "CLEANERS"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Blinds Cleaning/repairing");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Carpet & Rug Cleaning/repairing");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Duct Cleaning");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("House & Apartment Cleaning/maid Service");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);

            CheckBox chkTeamName5 = new CheckBox(getActivity());
            chkTeamName5.setText("Window & Gutter Cleaning");
            chkTeamName5.setTextColor(Color.BLACK);
            layout.addView(chkTeamName5);
        }else if(cat == "CARPENTER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Woodworking");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Stairs & railings");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Cabinetry & Millwork");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Framing");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);

            CheckBox chkTeamName5 = new CheckBox(getActivity());
            chkTeamName5.setText("Bathroom & kitchen");
            chkTeamName5.setTextColor(Color.BLACK);
            layout.addView(chkTeamName5);

        }else if(cat == "MOVERS") {
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Moving & Storage");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Junk Removal");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);
        }else if(cat == "FLOORING"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Carpet Installers");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Floor Laying & Refinishing");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Tile & Stone/Masonry & Bricklaying");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

        }else if(cat == "ROOFING"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Gutters & Eavestroughs");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Siding & Insulation");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Skylight installation/repairs");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

        }else if(cat == "HANDYMAN"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("General labour1");
            chkTeamName.setTextColor(Color.BLACK);
            layout.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("General labour2");
            chkTeamName2.setTextColor(Color.BLACK);
            layout.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("General labour3");
            chkTeamName3.setTextColor(Color.BLACK);
            layout.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("General labour4");
            chkTeamName4.setTextColor(Color.BLACK);
            layout.addView(chkTeamName4);
        }
    }

    @Override
    public void onClick(View v) {

        v.getId();
        switch (v.getId())
        {

            case R.id.btnNext:
                //HomeServiceTimeFragment dialog = HomeServiceTimeFragment.newInstance("dialog");
                //dialog.show(getChildFragmentManager(), "dialog");

//                hSDialog = new HomeServiceTimeFragment();
//                FragmentManager mgr = getActivity().getFragmentManager();
//                FragmentTransaction ft = mgr.beginTransaction();
//
//                hSDialog.show(ft,"hb");
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                HomeServiceTimeFragment modeDialog = HomeServiceTimeFragment.newInstance("hello");
                modeDialog.show(ft, "nj");

        }

    }
}