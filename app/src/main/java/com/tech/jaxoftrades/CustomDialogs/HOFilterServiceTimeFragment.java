package com.tech.jaxoftrades.CustomDialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.tech.jaxoftrades.R;


public class HOFilterServiceTimeFragment extends DialogFragment implements View.OnClickListener {

    Button btnStartServiceTime, btnEndServiceTime,btnSearchTradesman,btnSkipTradesman;
    View rootView;
    public static final int STARTIMEPICKER_FRAGMENT = 1;
    public static final int ENDTIMEPICKER_FRAGMENT = 2;
String Category="";
    Button btnOk,btnCancel;
    LinearLayout llCustomCheckBoxFilter;

    //for sending response about search and skip button selction
    public static final int SEARCH_BUTTON_SELECTED = 1;
    public static final int SKIP_BUTTON_SELECTED = 2;
    Intent i;

    DialogFragment pickTimeDialog;
    Fragment trList;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        rootView = inflater.inflate(R.layout.dialog_filter_service_time_fragment, null);

        init();

        builder.setView(rootView);
        return builder.create();
    }
    public static HOFilterServiceTimeFragment newInstance(String title) {
        HOFilterServiceTimeFragment frag = new HOFilterServiceTimeFragment();

        return frag;
    }


    private void init() {

        btnStartServiceTime = (Button) rootView.findViewById(R.id.btnStartServiceTime);
        btnEndServiceTime = (Button) rootView.findViewById(R.id.btnEndServiceTime);
        btnSearchTradesman = (Button) rootView.findViewById(R.id.btnSearchTradesman);
        btnSkipTradesman = (Button) rootView.findViewById(R.id.btnSkipTradesman);


        btnStartServiceTime.setOnClickListener(this);
        btnEndServiceTime.setOnClickListener(this);
        btnSearchTradesman.setOnClickListener(this);
        btnSkipTradesman.setOnClickListener(this);


        //For custom checkbox



        btnOk= (Button) rootView.findViewById(R.id.btnOk);
        btnCancel= (Button) rootView.findViewById(R.id.btnCancel);
        llCustomCheckBoxFilter= (LinearLayout) rootView.findViewById(R.id.llCustomCheckBoxFilter);
        btnOk.setOnClickListener(this);

        if(Category == "PAINTER") {
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Interior Painter");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Exterior Painter");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);
        }else if(Category == "PLUMBER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Drain Services");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Sprinkler Systems");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Water Filtration & Purification");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Septic Tank Cleaning,Repair & Installation");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);
        }else if(Category == "ELECTRICIAN"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Alarm Systems");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Lighting");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Appliance Repair");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Installing/Electrical Wiring");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);
        }else if(Category == "GARDNER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Fences & Gates Decks");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Lawn Maintenance");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Sprinkler Systems");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Swimming Pools, Spas & Hot Tubs Gazebos & Sheds");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);
        }else if(Category == "CLEANERS"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Blinds Cleaning/repairing");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Carpet & Rug Cleaning/repairing");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Duct Cleaning");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("House & Apartment Cleaning/maid Service");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);

            CheckBox chkTeamName5 = new CheckBox(getActivity());
            chkTeamName5.setText("Window & Gutter Cleaning");
            chkTeamName5.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName5);

        }else if(Category == "CARPENTER"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Woodworking");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Stairs & railings");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Cabinetry & Millwork");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("Framing");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);

            CheckBox chkTeamName5 = new CheckBox(getActivity());
            chkTeamName5.setText("Bathroom & kitchen");
            chkTeamName5.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName5);

        }else if(Category == "MOVERS") {
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Moving & Storage");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Junk Removal");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

        }else if(Category == "FLOORING"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Carpet Installers");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Floor Laying & Refinishing");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Tile & Stone/Masonry & Bricklaying");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

        }else if(Category == "ROOFING"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("Gutters & Eavestroughs");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("Siding & Insulation");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("Skylight installation/repairs");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

        }else if(Category == "HANDYMAN"){
            CheckBox chkTeamName = new CheckBox(getActivity());
            chkTeamName.setText("General labour1");
            chkTeamName.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName);

            CheckBox chkTeamName2 = new CheckBox(getActivity());
            chkTeamName2.setText("General labour2");
            chkTeamName2.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName2);

            CheckBox chkTeamName3 = new CheckBox(getActivity());
            chkTeamName3.setText("General labour3");
            chkTeamName3.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName3);

            CheckBox chkTeamName4 = new CheckBox(getActivity());
            chkTeamName4.setText("General labour4");
            chkTeamName4.setTextColor(Color.BLACK);
            llCustomCheckBoxFilter.addView(chkTeamName4);
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case STARTIMEPICKER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
                    Bundle bundle = data.getExtras();
                    String resultDate = bundle.getString("SelectedTime");
                    Log.v("From dialog fragment",resultDate);
                    btnStartServiceTime.setText(resultDate);
                }
                break;
            case ENDTIMEPICKER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
                    Bundle bundle = data.getExtras();
                    String resultDate = bundle.getString("SelectedTime");
                    Log.v("From dialog fragment",resultDate);
                    btnEndServiceTime.setText(resultDate);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnStartServiceTime:


                 pickTimeDialog = new PickingTimeDialog();
                pickTimeDialog.setTargetFragment(this, STARTIMEPICKER_FRAGMENT);
                pickTimeDialog.show(getFragmentManager().beginTransaction(), "TimePicker");

                break;
            case R.id.btnEndServiceTime:

                 pickTimeDialog = new PickingTimeDialog();
                pickTimeDialog.setTargetFragment(this, ENDTIMEPICKER_FRAGMENT);
                pickTimeDialog.show(getFragmentManager().beginTransaction(), "TimePicker");

                break;



        }

    }


}