package com.tech.jaxoftrades.CustomDialogs;


import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

import java.util.Calendar;


public class PickingTimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener{


    View dialogView;
    int args;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int amOrPm = c.get(Calendar.AM_PM);

        //Create and return a new instance of TimePickerDialog
//        return new TimePickerDialog(getActivity(),this, hour, minute,
//                DateFormat.is24HourFormat(getActivity()));
        return new TimePickerDialog(getActivity(), TimePickerDialog.THEME_HOLO_LIGHT,this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    //onTimeSet() callback method
    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        //Do something with the user chosen time
        //Get reference of host activity (XML Layout File) TextView widget
//        TextView tv = (TextView) getActivity().findViewById(R.id.tv);
//        //Set a message for user
//        tv.setText("Your chosen time is...\n\n");
//        //Display the user changed time on TextView
//        tv.setText(tv.getText()+ "Hour : " + String.valueOf(hourOfDay)
//                + "\nMinute : " + String.valueOf(minute) + "\n");

        String AMPM = "AM";
        if(hourOfDay>11)
        {
            hourOfDay = hourOfDay-12;
            AMPM = "PM";
        }
        String sendTime = (String.valueOf(hourOfDay)+" : "+ String.valueOf(minute)+" "+AMPM);

        Log.v("From dialog fragment",sendTime);

        Intent i =new Intent();
        i.putExtra("SelectedTime",sendTime);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,i);
    }

    }



