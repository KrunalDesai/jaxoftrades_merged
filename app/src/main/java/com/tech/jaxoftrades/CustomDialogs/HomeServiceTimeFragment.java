package com.tech.jaxoftrades.CustomDialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.Homeowner.HOSearchTrListFragment;


public class HomeServiceTimeFragment extends DialogFragment implements View.OnClickListener {

    Button btnStartServiceTime, btnEndServiceTime,btnSearchTradesman,btnSkipTradesman;
    View rootView;
    public static final int STARTIMEPICKER_FRAGMENT = 1;
    public static final int ENDTIMEPICKER_FRAGMENT = 2;

    //for sending response about search and skip button selction
    public static final int SEARCH_BUTTON_SELECTED = 1;
    public static final int SKIP_BUTTON_SELECTED = 2;
    Intent i;

    DialogFragment pickTimeDialog;
    Fragment trList;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        rootView = inflater.inflate(R.layout.dialog_searchtime, null);

        init();

        builder.setView(rootView);
        return builder.create();
    }
    public static HomeServiceTimeFragment newInstance(String title) {
        HomeServiceTimeFragment frag = new HomeServiceTimeFragment();

        return frag;
    }


    private void init() {

        btnStartServiceTime = (Button) rootView.findViewById(R.id.btnStartServiceTime);
        btnEndServiceTime = (Button) rootView.findViewById(R.id.btnEndServiceTime);
        btnSearchTradesman = (Button) rootView.findViewById(R.id.btnSearchTradesman);
        btnSkipTradesman = (Button) rootView.findViewById(R.id.btnSkipTradesman);


        btnStartServiceTime.setOnClickListener(this);
        btnEndServiceTime.setOnClickListener(this);
        btnSearchTradesman.setOnClickListener(this);
        btnSkipTradesman.setOnClickListener(this);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case STARTIMEPICKER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
                    Bundle bundle = data.getExtras();
                    String resultDate = bundle.getString("SelectedTime");
                    Log.v("From dialog fragment",resultDate);
                    btnStartServiceTime.setText(resultDate);
                }
                break;
            case ENDTIMEPICKER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
                    Bundle bundle = data.getExtras();
                    String resultDate = bundle.getString("SelectedTime");
                    Log.v("From dialog fragment",resultDate);
                    btnEndServiceTime.setText(resultDate);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnStartServiceTime:


                 pickTimeDialog = new PickingTimeDialog();
                pickTimeDialog.setTargetFragment(this, STARTIMEPICKER_FRAGMENT);
                pickTimeDialog.show(getFragmentManager().beginTransaction(), "TimePicker");

                break;
            case R.id.btnEndServiceTime:

                 pickTimeDialog = new PickingTimeDialog();
                pickTimeDialog.setTargetFragment(this, ENDTIMEPICKER_FRAGMENT);
                pickTimeDialog.show(getFragmentManager().beginTransaction(), "TimePicker");

                break;

            case R.id.btnSearchTradesman:


                Toast.makeText(getActivity(),"Tradesman List", Toast.LENGTH_SHORT).show();
//
//                trList = new HomeownerTrListFragment();
//
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.frame, trList, trList.getClass().getSimpleName()).addToBackStack(null).commit();
//
////                 i =new Intent();
////                i.putExtra("SelectedButton",SEARCH_BUTTON_SELECTED);
//                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,getActivity().getIntent());

                break;
            case R.id.btnSkipTradesman:

                Toast.makeText(getActivity(),"Tradesman List", Toast.LENGTH_SHORT).show();
//                trList = new HoSubcategorySelection();
//
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.frame, trList, trList.getClass().getSimpleName()).addToBackStack(null).commit();

//
//                i=new Intent();
//                i.putExtra("SelectedButton",SKIP_BUTTON_SELECTED);
//                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,getActivity().getIntent());

                Intent i= new Intent(getActivity(),HOSearchTrListFragment.class);
                startActivity(i);


                break;


        }

    }


}