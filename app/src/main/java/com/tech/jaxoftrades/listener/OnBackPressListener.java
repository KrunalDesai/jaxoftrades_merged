package com.tech.jaxoftrades.listener;

/**
 * Created by arbaz on 20/5/17.
 */

public interface OnBackPressListener { boolean onBackPressed();
}
