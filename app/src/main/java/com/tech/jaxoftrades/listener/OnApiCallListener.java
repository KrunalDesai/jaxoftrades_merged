package com.tech.jaxoftrades.listener;

/**
 * Created by arbaz on 9/6/17.
 */

public interface OnApiCallListener {

    void onSuccess(int responseCode, String responseString, String requestType);

    void onFailure(String errorMessage);
}
