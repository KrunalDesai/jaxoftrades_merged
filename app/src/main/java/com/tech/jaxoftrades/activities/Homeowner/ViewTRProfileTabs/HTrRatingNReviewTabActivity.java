package com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.HomeOwner.TrViewProfileTabsAdapter.HTrRatingNReviewTabAdapter;
import com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel.HTrProfileTabObject;

import java.util.ArrayList;


public class HTrRatingNReviewTabActivity extends AppCompatActivity {

    HTrRatingNReviewTabAdapter mAdapter;
    RecyclerView mRecyclerView;
    // Toolbar tblBar;

    ArrayList<HTrProfileTabObject> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hviewprofilegallery);

        data.add(new HTrProfileTabObject("Anku",3.2f,"Very Good","Good service","12-07-2017"));
        data.add(new HTrProfileTabObject("Anku1",3.8f,"Very Good","Good service","12-07-2017"));
        data.add(new HTrProfileTabObject("Anku2",3.5f,"Very Good","Good service","12-07-2017"));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Ratings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerView.setHasFixedSize(true);


        mAdapter = new HTrRatingNReviewTabAdapter(HTrRatingNReviewTabActivity.this, data);
        mRecyclerView.setAdapter(mAdapter);

//        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
//                new RecyclerItemClickListener.OnItemClickListener() {
//
//                    @Override
//                    public void onItemClick(View view, int position) {
//
//                        Intent intent = new Intent(HTrRatingNReviewTabActivity.this, HTrGalleryDetailActivity.class);
//                        intent.putParcelableArrayListExtra("data", data);
//                        intent.putExtra("pos", position);
//                        startActivity(intent);
//
//                    }
//                }));
//
//    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // app icon in action bar clicked; goto parent activity.
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
    }
}

