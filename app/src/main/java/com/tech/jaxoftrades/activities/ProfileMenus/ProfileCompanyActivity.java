package com.tech.jaxoftrades.activities.ProfileMenus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.CompanyProfileListAdapter;
import com.tech.jaxoftrades.listener.RecyclerItemClickListener;
import com.tech.jaxoftrades.model.Common.CompanyProfileListMain;

import java.util.ArrayList;

public class ProfileCompanyActivity extends AppCompatActivity implements View.OnClickListener {

    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle, tbSave;
    ImageView tbBack;

    //Related To Activity
    RecyclerView rvComProfile;

    CompanyProfileListAdapter companyProfileListAdapter;
    ArrayList<CompanyProfileListMain> companyProfileListMainArrayList;
    CompanyProfileListMain companyProfileListMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_company);
        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbSave = (TextView) toolbar.findViewById(R.id.tbSave);
        tbTitle.setText(getString(R.string.TmProPlumbingTxt));
        tbBack.setVisibility(View.VISIBLE);
        tbSave.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        tbSave.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        rvComProfile = (RecyclerView) findViewById(R.id.rvComProfile);
        rvComProfile.setLayoutManager(new LinearLayoutManager(this));

        companyProfileListMainArrayList=new ArrayList<>();
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileContactInfo)));
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileAbout)));
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileSpecializeIn)));
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileRecentProject)));
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileGallery)));
        companyProfileListMainArrayList.add(new CompanyProfileListMain(getString(R.string.TmProSCProfileSocialLink)));


        companyProfileListAdapter = new CompanyProfileListAdapter(companyProfileListMainArrayList, this);
        rvComProfile.setAdapter(companyProfileListAdapter);

        rvComProfile.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                companyProfileListMain = companyProfileListMainArrayList.get(position);
                try {
                    if (companyProfileListMain.getItemName() != null && !companyProfileListMain.getItemName().isEmpty()) {

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.tbSave:
                finish();
                break;

            default:
                break;
        }

    }
}

