package com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;


import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.HOFragments.HomeRequestDailogeFragment;


public class HOViewTRProfileTabActivity extends AppCompatActivity {


    CollapsingToolbarLayout ctbLayout;
    Toolbar toolbar;
    View typeView;
    SharedPreferences pref;
 Button btnRequest;




    void initViews(){



        ctbLayout = (CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);

      //  typeView = findViewById(R.id.typeView);



        ctbLayout.setTitle("Jax of Trades");
        ctbLayout.setTitleEnabled(false);
        btnRequest = (Button)findViewById(R.id.buttonReq);
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog();
            }
        });



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_htrviewprofiletabactivity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        initViews();

    }


    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        HomeRequestDailogeFragment reDialoge = HomeRequestDailogeFragment.newInstance("Some Title");
        reDialoge.show(fm, "title");
    }




    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}




