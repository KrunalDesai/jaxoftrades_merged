package com.tech.jaxoftrades.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;

public class EditProjectActivity extends AppCompatActivity implements View.OnClickListener {


    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;
    ImageView tbBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);


        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbTitle.setText(getString(R.string.TmRPDEditProjectTitle));
        tbBack.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;

            default:
                break;
        }
    }
}