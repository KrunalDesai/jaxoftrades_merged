package com.tech.jaxoftrades.activities.SettingsMenus;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {


    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;
    ImageView tbBack;


    EditText etTmOldPasss,
            etTmNewPass,
            etTmConfirmPass;
    Button btnTmPassSave;

    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    //UserResponse userResponse;
    String strLoginResponse;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cahnage_password);

        apiFunctions = new ApiFunctions(this, this);
        //getting data
        strLoginResponse = Global.getPreference("UserResponseData", "");
       // userResponse = gson.fromJson(strLoginResponse.toString(), UserResponse.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbTitle.setText(getString(R.string.TmSettingCPassword));
        tbBack.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        etTmOldPasss = (EditText) toolbar.findViewById(R.id.etTmOldPasss);
        etTmNewPass = (EditText) toolbar.findViewById(R.id.etTmNewPass);
        etTmConfirmPass = (EditText) toolbar.findViewById(R.id.etTmConfirmPass);
        btnTmPassSave = (Button) toolbar.findViewById(R.id.btnTmPassSave);
        btnTmPassSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.btnTmPassSave:
                try {
                    String oldPass = etTmOldPasss.getText().toString();
                    String NewPass = etTmNewPass.getText().toString();
                    String ConfirmPass = etTmConfirmPass.getText().toString();
                    if (!TextUtils.isEmpty(oldPass) && !TextUtils.isEmpty(NewPass) && !TextUtils.isEmpty(ConfirmPass)) {
                        if (NewPass.equals(ConfirmPass)) {
//                            registerRequest = new RegisterRequest(email, fName, lName, password, contactNumber, companyName, Constants.TYPE_TRADESMEN);
//                            AppDialog.showProgressDialog(SignUpActivity.this);
//                            apiFunctions.userRegister(Api.MainUrl + Api.ActionRegister, registerRequest);
                        } else {
                            AppDialog.showAlertDialog(ChangePasswordActivity.this, null, getString(R.string.PasswordMatchError),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    } else {
                        AppDialog.showAlertDialog(ChangePasswordActivity.this, null, getString(R.string.ChangePasswordError),
                                getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            default:
                break;
        }

    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        Log.e("Change Password Success" + responseString);
        AppDialog.dismissProgressDialog();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Api.data);
                    if (jsonArray.length() > 0) {
                       // userResponse = gson.fromJson(jsonArray.get(0).toString(), UserResponse.class);


                    }

                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                    //display error message as per response
                } else {
                    //when response code not match default message will display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {

                            AppDialog.showAlertDialog(ChangePasswordActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(ChangePasswordActivity.this, null, apiResponse.getERR_MSG(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(ChangePasswordActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        Log.e("Change Password Failure" + errorMessage);
        AppDialog.dismissProgressDialog();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(ChangePasswordActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });
    }
}
