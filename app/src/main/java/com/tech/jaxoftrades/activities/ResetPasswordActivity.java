package com.tech.jaxoftrades.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tech.jaxoftrades.R;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private android.widget.EditText etTMNewPass;
    private android.widget.EditText etTMConfirmPass;
    private android.widget.Button btnResetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        this.btnResetPassword = (Button) findViewById(R.id.btnResetPassword);
        this.etTMConfirmPass = (EditText) findViewById(R.id.etTMConfirmPass);
        this.etTMNewPass = (EditText) findViewById(R.id.etTMNewPass);
        btnResetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnResetPassword:
                Intent iLogin = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(iLogin);
                finish();
                break;
            default:
                break;


        }
    }
}
