package com.tech.jaxoftrades.activities.Homeowner;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.LoginActivity;
import com.tech.jaxoftrades.adapter.HomeOwner.HOLeftMenuAdapter;
import com.tech.jaxoftrades.fragments.HOLeftMenu.HOAboutUsFragment;
import com.tech.jaxoftrades.fragments.HOLeftMenu.HOHistoryFragment;
import com.tech.jaxoftrades.fragments.HOLeftMenu.HOHomefragment;
import com.tech.jaxoftrades.fragments.HOLeftMenu.HOProfileFragment;
import com.tech.jaxoftrades.fragments.HOLeftMenu.TestimonialFragment;
import com.tech.jaxoftrades.listener.RecyclerItemClickListener;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeOwnerLeftMenuMain;

import java.util.ArrayList;

;

public class HomeOwnerDashboardActivity extends AppCompatActivity implements View.OnClickListener {
    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;

    //Related To Header
    SimpleDraweeView sdTMUserProfile;
    TextView tvTMUserName;

    //Related To Adapter
    HOLeftMenuAdapter leftMenuAdapter;
    HomeOwnerLeftMenuMain leftMenuMain;
    ArrayList<HomeOwnerLeftMenuMain> leftMenuMainArrayList;

    //Related To Activity
    RecyclerView rvLeftMenu;
    TextView tvLogout;

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_dashboard);

        //Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbTitle.setText(getString(R.string.TMLMHome));
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);


        //set Default Fragment Here
        fragment = new HOHomefragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.TMContainer, fragment)
                .commit();

        //Related To Activity
        tvLogout = (TextView) findViewById(R.id.tvLogout);
        rvLeftMenu = (RecyclerView) findViewById(R.id.rvLeftMenu);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        rvLeftMenu.setLayoutManager(mLayoutManager1);

        tvLogout.setOnClickListener(this);
        leftMenuMainArrayList = new ArrayList<>();
        leftMenuMainArrayList.add(new HomeOwnerLeftMenuMain(getString(R.string.HOLMHome)));
        leftMenuMainArrayList.add(new HomeOwnerLeftMenuMain(getString(R.string.HOLMProfile)));
        leftMenuMainArrayList.add(new HomeOwnerLeftMenuMain(getString(R.string.HOLMHistory)));
        leftMenuMainArrayList.add(new HomeOwnerLeftMenuMain(getString(R.string.HOLMTestimonial)));
        leftMenuMainArrayList.add(new HomeOwnerLeftMenuMain(getString(R.string.HOLMAboutus)));

        leftMenuAdapter = new HOLeftMenuAdapter(leftMenuMainArrayList, getApplicationContext());
        rvLeftMenu.setAdapter(leftMenuAdapter);

        //Related To Left Menu
        rvLeftMenu.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                leftMenuMain = leftMenuMainArrayList.get(position);


                try {
                    if (leftMenuMain.getStrHOMenuTitle().equals(getString(R.string.HOLMHome))) {
                        changeFragment(new HOHomefragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrHOMenuTitle().equals(getString(R.string.HOLMProfile))) {
                        changeFragment(new HOProfileFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    }else if (leftMenuMain.getStrHOMenuTitle().equals(getString(R.string.HOLMHistory))) {
                        changeFragment(new HOHistoryFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrHOMenuTitle().equals(getString(R.string.HOLMTestimonial))) {
                        changeFragment(new TestimonialFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrHOMenuTitle().equals(getString(R.string.HOLMAboutus))) {
                        changeFragment(new HOAboutUsFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    void changeFragment(Fragment fragment) {
        try {
            String backStateName = fragment.getClass().getName();
            String fragmentTag = backStateName;

            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);


            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                fragmentTransaction.replace(R.id.TMContainer, fragment, fragmentTag);
                fragmentTransaction.addToBackStack(backStateName);
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvLogout:
                startActivity(new Intent(this,LoginActivity.class));
                break;
            default:
                break;
        }
    }
}
