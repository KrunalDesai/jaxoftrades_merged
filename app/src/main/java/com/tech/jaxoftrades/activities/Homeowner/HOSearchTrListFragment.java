package com.tech.jaxoftrades.activities.Homeowner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.HomeOwner.HomeownerTrListAdapter;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeOwnerTrList;

import java.util.ArrayList;
import java.util.List;


public class HOSearchTrListFragment extends AppCompatActivity {


    public static final int SEARCH_BUTTON_SELECTED = 1;
    public static final int SKIP_BUTTON_SELECTED = 2;

    private List<HomeOwnerTrList> trLists;
    private RecyclerView rvHomeTradeList;
    private HomeownerTrListAdapter mAdapter;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ho_search_tr_list);



        rvHomeTradeList = (RecyclerView) findViewById(R.id.rvHomeTradeList);


        trLists = new ArrayList<>();
        mAdapter = new HomeownerTrListAdapter(getApplicationContext(),trLists);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HOSearchTrListFragment.this);
        rvHomeTradeList.setLayoutManager(mLayoutManager);

        rvHomeTradeList.setItemAnimator(new DefaultItemAnimator());
        rvHomeTradeList.setAdapter(mAdapter);

        prepareMovieData();
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SEARCH_BUTTON_SELECTED:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
//                    Bundle bundle = data.getExtras();
//                    int resultDate = bundle.getInt("SelectedButton");
//                    Log.v("From dialog fragment",String.valueOf(resultDate));
                    Log.v("From dialog fragment","tesing 1");

                }
                break;
            case SKIP_BUTTON_SELECTED:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
//                    Bundle bundle = data.getExtras();
//                    int resultDate1 = bundle.getInt("SelectedTime");
//                    Log.v("From dialog fragment",String.valueOf(resultDate1));
                    Log.v("From dialog fragment","tesing 2");

                }
                break;
        }
    }


    private void prepareMovieData() {

        HomeOwnerTrList trListData = new HomeOwnerTrList("David Smith", "Plumber - Home/commercial", "Toronto Plumbing Inc.","Available from 8am to 6pm","0.5","3.5");
        trLists.add(trListData);

        trListData = new HomeOwnerTrList("Thomas Brown", "Plumber - Home/commercial", "Ontario Plumbing Inc.","Available from 10am to 2pm","1.5","3.5");
        trLists.add(trListData);

        trListData = new HomeOwnerTrList("Sarah Hill", "Plumber - Home/commercial", "Brampton Plumbing Inc.","Available from 11am to 5pm","2.0","3.5");
        trLists.add(trListData);

        trListData = new HomeOwnerTrList("James Fisher", "Plumber - Home/commercial", "London Plumbing Inc.","Available from 11am to 6pm","2.5","2.5");
        trLists.add(trListData);



        mAdapter.notifyDataSetChanged();

    }
}
