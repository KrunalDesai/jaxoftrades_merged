package com.tech.jaxoftrades.activities.Homeowner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.tech.jaxoftrades.R;

public class SignUpHomeOwnerActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etTMSignEMail;
    private EditText etTMSignFName;
    private EditText etTMSignLName;
    private EditText etTMSignPassword;
    private EditText etTMSignConPassword;
    private EditText etTMSignContactNo;
    private EditText etTMSignCompanyName;
    private CheckBox cbTMSignPnT;
    private TextView tvTMPnT;
    private Button btnTMNext;

    private String tempTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeowner_sign_up);


        this.tvTMPnT = (TextView) findViewById(R.id.tvTMPnT);
        this.cbTMSignPnT = (CheckBox) findViewById(R.id.cbTMSignPnT);
        this.etTMSignCompanyName = (EditText) findViewById(R.id.etTMSignCompanyName);
        this.etTMSignContactNo = (EditText) findViewById(R.id.etTMSignContactNo);
        this.etTMSignConPassword = (EditText) findViewById(R.id.etTMSignConPassword);
        this.etTMSignPassword = (EditText) findViewById(R.id.etTMSignPassword);
        this.etTMSignLName = (EditText) findViewById(R.id.etTMSignLName);
        this.etTMSignFName = (EditText) findViewById(R.id.etTMSignFName);
        this.etTMSignEMail = (EditText) findViewById(R.id.etTMSignEMail);
        this.btnTMNext = (Button) findViewById(R.id.btnTMNext);

        try {
            tvTMPnT.setText(Html.fromHtml(getString(R.string.TMSignInPnTText)));
        } catch (Exception e) {

        }

        btnTMNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTMNext:
                Intent iEditProfile=new Intent(SignUpHomeOwnerActivity.this,HomeOwnerDashboardActivity.class);
                startActivity(iEditProfile);

                break;
            default:
                break;
        }

    }
}
