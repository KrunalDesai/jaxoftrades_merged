package com.tech.jaxoftrades.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;

import java.util.ArrayList;

public class NotificationDetailActivity extends AppCompatActivity implements View.OnClickListener {

    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;
    ImageView tbBack;

    LinearLayout llRvNotification, llNotiEmpty;
    ArrayList<String> stringArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);
        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbTitle.setText(getString(R.string.TmRPNDTitle));
        tbBack.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        llRvNotification = (LinearLayout) findViewById(R.id.llRvNotification);
        llNotiEmpty = (LinearLayout) findViewById(R.id.llNotiEmpty);

        stringArrayList = new ArrayList<>();
        if (!stringArrayList.isEmpty()) {
            llRvNotification.setVisibility(View.VISIBLE);
            llNotiEmpty.setVisibility(View.GONE);
        } else {
            llRvNotification.setVisibility(View.GONE);
            llNotiEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;

            default:
                break;
        }
    }
}
