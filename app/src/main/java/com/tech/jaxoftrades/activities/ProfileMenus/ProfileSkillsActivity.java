package com.tech.jaxoftrades.activities.ProfileMenus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.Common.TagSkills;
import com.tech.jaxoftrades.utils.TagVIew.TagCloudLinkView;

import java.util.ArrayList;
import java.util.List;

public class ProfileSkillsActivity extends AppCompatActivity implements View.OnClickListener {
    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle, tbSave;
    ImageView tbBack;

    EditText etAddSkill;

    //Related to Add skills layout
    List<String> skillsArrayList;
    LinearLayout llAddSkills;
    LinearLayout llAddSkillsLayout;
    Button btnAddSkill;
    TagSkills tagSkills;
    TagCloudLinkView skillTagView;
    int tagId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_skills);


        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbSave = (TextView) toolbar.findViewById(R.id.tbSave);
        tbTitle.setText("Add" + " " + getString(R.string.TmProSkills));
        tbBack.setVisibility(View.VISIBLE);
        tbSave.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        tbSave.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        etAddSkill = (EditText) findViewById(R.id.etAddSkill);
        btnAddSkill = (Button) findViewById(R.id.btnAddSkill);
        llAddSkills = (LinearLayout) findViewById(R.id.llAddSkills);
        skillTagView = (TagCloudLinkView) findViewById(R.id.skillTagView);

        btnAddSkill.setOnClickListener(this);
        skillsArrayList=new ArrayList<>();

        //For click event
        skillTagView.setOnTagSelectListener(new TagCloudLinkView.OnTagSelectListener() {
            @Override
            public void onTagSelected(TagSkills tag, int i) {
//                Toast.makeText(getActivity(), tag.getText() + "=" + i, Toast.LENGTH_SHORT).show();
            }
        });
        //For Delete
        skillTagView.setOnTagDeleteListener(new TagCloudLinkView.OnTagDeleteListener() {

            @Override
            public void onTagDeleted(TagSkills tag, int i) {
//                Toast.makeText(getActivity(),"Position" + i, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.tbSave:
                finish();
                break;
            case R.id.btnAddSkill:
                String skillName = etAddSkill.getText().toString();
                tagSkills = new TagSkills();
                tagSkills = new TagSkills(tagId++, skillName);
                if (!TextUtils.isEmpty(skillName)) {
                    skillTagView.add(tagSkills);
                    skillTagView.drawTags();
                    etAddSkill.setText("");
                } else {
                    etAddSkill.setError("Please enter your skill!");
                }
                break;
            case R.id.ivSkillCancel:

                break;
            default:
                break;
        }

    }

}
