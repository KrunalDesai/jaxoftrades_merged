package com.tech.jaxoftrades.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.Homeowner.SignUpHomeOwnerActivity;

public class SelectionActivity extends AppCompatActivity implements View.OnClickListener {

    private com.facebook.drawee.view.SimpleDraweeView sdTMSSTradesMan;
    private com.facebook.drawee.view.SimpleDraweeView sdTMSSHomeOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        this.sdTMSSHomeOwner = (SimpleDraweeView) findViewById(R.id.sdTMSSHomeOwner);
        this.sdTMSSTradesMan = (SimpleDraweeView) findViewById(R.id.sdTMSSTradesMan);
        sdTMSSHomeOwner.setOnClickListener(this);
        sdTMSSTradesMan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sdTMSSHomeOwner:
//                Toast.makeText(this, "Home Owner Side Coming Soon", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(SelectionActivity.this, SignUpHomeOwnerActivity.class));
                break;
            case R.id.sdTMSSTradesMan:
                startActivity(new Intent(SelectionActivity.this, SignUpActivity.class));
                break;
        }

    }
}
