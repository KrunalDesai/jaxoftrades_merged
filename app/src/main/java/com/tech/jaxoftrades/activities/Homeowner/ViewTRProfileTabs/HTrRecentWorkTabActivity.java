package com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.HomeOwner.TrViewProfileTabsAdapter.HTrRencentWorkTabAdapter;
import com.tech.jaxoftrades.listener.RecyclerItemClickListener;
import com.tech.jaxoftrades.model.HomOwnerModel.TrViewProfileTabModel.HTrProfileTabObject;

import java.util.ArrayList;


public class HTrRecentWorkTabActivity extends AppCompatActivity {

    HTrRencentWorkTabAdapter mAdapter;
    RecyclerView mRecyclerView;
    public static String IMGS[] = {
            "https://images.unsplash.com/photo-1444090542259-0af8fa96557e?q=80&fm=jpg&w=1080&fit=max&s=4b703b77b42e067f949d14581f35019b",
            "http://hdwallpaperia.com/wp-content/uploads/2013/11/Skull-Designs-Wallpaper-640x400.jpg",
            "https://images.unsplash.com/photo-1437651025703-2858c944e3eb?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1431538510849-b719825bf08b?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1434873740857-1bc5653afda8?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1439396087961-98bc12c21176?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1433616174899-f847df236857?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1438480478735-3234e63615bb?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300",
            "https://images.unsplash.com/photo-1438027316524-6078d503224b?dpr=2&fit=crop&fm=jpg&h=725&q=50&w=1300"
    };
    // Toolbar tblBar;

    ArrayList<HTrProfileTabObject> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hr_view_tr_profile_gallery);

        data.add(new HTrProfileTabObject("Interior Painter","Please board the afternoon buses as per your class schedule. For example: If your class doesn't start until 6:00 pm please give preference to the.",IMGS));
        data.add(new HTrProfileTabObject("Exterior Painter","Please board the afternoon buses as per your class schedule. For example: If your class doesn't start until 6:00 pm please give preference to the.",IMGS));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Recent Project Work");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerView.setHasFixedSize(true);


        mAdapter = new HTrRencentWorkTabAdapter(HTrRecentWorkTabActivity.this, data);
        mRecyclerView.setAdapter(mAdapter);


        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent i = new Intent(HTrRecentWorkTabActivity.this,HORecentProjectDetailActivity.class);
                        startActivity(i);
                    }
                }));


//        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
//                new RecyclerItemClickListener.OnItemClickListener() {
//
//                    @Override
//                    public void onItemClick(View view, int position) {
//
//                        Intent intent = new Intent(HTrRatingNReviewTabActivity.this, HTrGalleryDetailActivity.class);
//                        intent.putParcelableArrayListExtra("data", data);
//                        intent.putExtra("pos", position);
//                        startActivity(intent);
//
//                    }
//                }));
//
//    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // app icon in action bar clicked; goto parent activity.
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
    }
}