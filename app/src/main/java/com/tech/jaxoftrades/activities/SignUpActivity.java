package com.tech.jaxoftrades.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.TradesMan.TMEditProfileActivity;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Constants;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {

    private android.widget.EditText etTMSignEMail;
    private android.widget.EditText etTMSignFName;
    private android.widget.EditText etTMSignLName;
    private android.widget.EditText etTMSignPassword;
    private android.widget.EditText etTMSignConPassword;
    private android.widget.EditText etTMSignContactNo;
    private android.widget.EditText etTMSignCompanyName;
    private android.widget.CheckBox cbTMSignPnT;
    private android.widget.TextView tvTMPnT;
    private android.widget.Button btnTMNext;

    private String tempTxt;

    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        apiFunctions = new ApiFunctions(this, this);
        this.tvTMPnT = (TextView) findViewById(R.id.tvTMPnT);
        this.cbTMSignPnT = (CheckBox) findViewById(R.id.cbTMSignPnT);
        this.etTMSignEMail = (EditText) findViewById(R.id.etTMSignEMail);
        this.etTMSignFName = (EditText) findViewById(R.id.etTMSignFName);
        this.etTMSignLName = (EditText) findViewById(R.id.etTMSignLName);
        this.etTMSignPassword = (EditText) findViewById(R.id.etTMSignPassword);
        this.etTMSignConPassword = (EditText) findViewById(R.id.etTMSignConPassword);
        this.etTMSignContactNo = (EditText) findViewById(R.id.etTMSignContactNo);
        this.etTMSignCompanyName = (EditText) findViewById(R.id.etTMSignCompanyName);
        this.btnTMNext = (Button) findViewById(R.id.btnTMNext);

        try {
            tvTMPnT.setText(Html.fromHtml(getString(R.string.TMSignInPnTText)));
        } catch (Exception e) {

        }

        btnTMNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTMNext:
                try {
                    String type = Constants.TYPE_TRADESMEN;
                    String email = etTMSignEMail.getText().toString();
                    String fName = etTMSignFName.getText().toString();
                    String lName = etTMSignLName.getText().toString();
                    String password = etTMSignPassword.getText().toString();
                    String cPassword = etTMSignConPassword.getText().toString();
                    String contactNumber = etTMSignContactNo.getText().toString();
                    String companyName = etTMSignCompanyName.getText().toString();

                    if (Global.isNetworkAvailable(this)) {
                        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(fName) && !TextUtils.isEmpty(lName) && !TextUtils.isEmpty(contactNumber)) {
                            if (password.equals(cPassword)) {
                                if (cbTMSignPnT.isChecked()){
                                    AppDialog.showProgressDialog(SignUpActivity.this);
                                    apiFunctions.userRegister(Api.MainUrl + Api.ActionRegister, type, email, fName, lName, cPassword, contactNumber);
                                }else {
                                    Toast.makeText(this,getString(R.string.TMSignInppandterms),Toast.LENGTH_LONG).show();
                                }

                            } else {
                                AppDialog.showAlertDialog(SignUpActivity.this, null, getString(R.string.PasswordMatchError),
                                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                            }

                        } else {
                            AppDialog.showAlertDialog(SignUpActivity.this, null, getString(R.string.RegisterError),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    } else {
                        AppDialog.noNetworkDialog(this, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                    Intent iEditProfile = new Intent(SignUpActivity.this, TMEditProfileActivity.class);
//                    startActivity(iEditProfile);
//                    Global.activityTransition(SignUpActivity.this);
                break;
            default:
                break;
        }

    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        Log.e("Register Success" + responseString);
        AppDialog.dismissProgressDialog();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Api.data);
                    if (jsonArray.length() > 0) {
                        userResponse = gson.fromJson(jsonArray.get(0).toString(), UserResponse.class);
                        strUserResponse = gson.toJson(userResponse, UserResponse.class);
                    }

                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                    //display error message as per response
                } else {
                    //when response code not match default message will display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {

                            AppDialog.showAlertDialog(SignUpActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Global.storePreference("UserResponseData", strUserResponse);
                                            Global.storePreference("IsLogin", true);
                                            dialogInterface.dismiss();

                                            Intent iEditProfile = new Intent(SignUpActivity.this, TMEditProfileActivity.class);
                                            iEditProfile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(iEditProfile);
                                            Global.activityTransition(SignUpActivity.this);
                                        }
                                    });

                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(SignUpActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(SignUpActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        Log.e("Register Failure" + errorMessage);
        AppDialog.dismissProgressDialog();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(SignUpActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });
    }
}
