package com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.tech.jaxoftrades.R;


public class TradesmenProfileToHOActivity extends TabActivity {
    TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiletradesmen_to_ho);
        tabHost = getTabHost();

        init();
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
              //  tabHost.setBackgroundResource(R.drawable.ic_settings_black_24dp);
            }
        });
    }

    private void init() {
        Intent intent;
//        intent = new Intent().setClass(this, HTrViewProfileTabActivity.class);
//        tabHost.addTab(tabHost
//                .newTabSpec("OffersActivityGroup")
//                .setIndicator("",
//                        getResources().getDrawable(R.drawable.ic_lay))
//                .setContent(intent));

        tabHost.addTab(tabHost
                .newTabSpec(null)
                .setIndicator(null,
                        getResources().getDrawable(R.drawable.ic_ho_tab_view_profile))
                .setContent(new Intent(this, HOViewTRProfileTabActivity.class)));
        tabHost.setCurrentTab(0);

        tabHost.addTab(tabHost
                .newTabSpec(null)
                .setIndicator(null,
                        getResources().getDrawable(R.drawable.ic_ho_tab_view_recent_projects))
                .setContent(new Intent(this, HTrRecentWorkTabActivity.class)));
        tabHost.setCurrentTab(0);

        tabHost.addTab(tabHost
                .newTabSpec(null)
                .setIndicator(null,
                        getResources().getDrawable(R.drawable.ic_ho_tab_view_gallery))
                .setContent(new Intent(this, HOViewProfileGalleryActivity.class)));
        tabHost.setCurrentTab(0);

        tabHost.addTab(tabHost
                .newTabSpec(null)
                .setIndicator(null,
                        getResources().getDrawable(R.drawable.ic_ho_tab_view_ratings))
                .setContent(new Intent(this, HTrRatingNReviewTabActivity.class)));
        tabHost.setCurrentTab(0);

        for(int i = 0; i < tabHost.getTabWidget().getTabCount(); i++)
        {
           // tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.transparent));
        }

    }



}
