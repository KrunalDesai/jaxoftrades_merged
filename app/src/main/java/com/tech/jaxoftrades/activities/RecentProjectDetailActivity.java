package com.tech.jaxoftrades.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.ProjectImageAdapter;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.utils.PageIndicator.CirclePageIndicator;

import java.util.ArrayList;


public class RecentProjectDetailActivity extends AppCompatActivity implements View.OnClickListener {


    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;
    ImageView tbBack;

    //For image slider

    ViewPager vpRPImages;
    int count = 0;
    CirclePageIndicator indicator;

    ScrollView svProjectDetails;

    ProjectImageAdapter projectImageAdapter;
    ArrayList<String> stringArrayList;

    FloatingActionButton fab, fab_delete, fab_edit;
    Animation show_fab1, show_fab2;
    Animation hide_fab1, hide_fab2;

    boolean isFABOpen;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_project_detail);


        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbTitle.setText(getString(R.string.TmRPDTitle));
        tbBack.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        vpRPImages = (ViewPager) findViewById(R.id.vpRPImages);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab_delete = (FloatingActionButton) findViewById(R.id.fab_delete);
        fab_edit = (FloatingActionButton) findViewById(R.id.fab_edit);
        svProjectDetails = (ScrollView) findViewById(R.id.svProjectDetails);
        stringArrayList = new ArrayList<>();
        //setting image slider
        setData();

        //Animations
        //fab1
        show_fab1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show);
        hide_fab1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide);
        //fab2
        show_fab2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_show);
        hide_fab2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_hide);


        fab.setOnClickListener(this);
        fab_delete.setOnClickListener(this);
        fab_edit.setOnClickListener(this);


    }

    private void setData() {

        //tvTVDName.setText(templeMain.getTempleName());


        vpRPImages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                count = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                stringArrayList.add("");
                stringArrayList.add("");
                stringArrayList.add("");
                stringArrayList.add("");

                projectImageAdapter = new ProjectImageAdapter(RecentProjectDetailActivity.this, stringArrayList);
                vpRPImages.setAdapter(projectImageAdapter);

                indicator.setViewPager(vpRPImages);

                /*vpHotelIamge.setCurrentItem(0);*/
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.fab:
                if (!isFABOpen) {
                    showMenu();
                } else {
                    hideMenu();
                }
                break;
            case R.id.fab_delete:
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AppDialog.showAlertDialog(RecentProjectDetailActivity.this, null, getString(R.string.TmRPDDeleteDialog), getString(R.string.txt_ok), getString(R.string.txt_cancel), onPositiveClickListener, onNegativeClickListener);
                    }
                });


                break;
            case R.id.fab_edit:
                startActivity(new Intent(this, EditProjectActivity.class));

                break;
            default:
                break;
        }

    }

    public void showMenu() {
        isFABOpen = true;
        //For Fab1 menu
        FrameLayout.LayoutParams show_layoutParams_fab_1 = (FrameLayout.LayoutParams) fab_delete.getLayoutParams();
        show_layoutParams_fab_1.rightMargin += (int) (fab_delete.getWidth() * 1.7);
        show_layoutParams_fab_1.bottomMargin += (int) (fab_delete.getHeight() * 0.25);
        fab_delete.setLayoutParams(show_layoutParams_fab_1);
        fab_delete.startAnimation(show_fab1);
        fab_delete.setClickable(true);

        //For Fab2 menu
        FrameLayout.LayoutParams show_layoutParams_fab_2 = (FrameLayout.LayoutParams) fab_edit.getLayoutParams();
        show_layoutParams_fab_2.rightMargin += (int) (fab_edit.getWidth() * 0.25);
        show_layoutParams_fab_2.bottomMargin += (int) (fab_edit.getHeight() * 1.7);
        fab_edit.setLayoutParams(show_layoutParams_fab_2);
        fab_edit.startAnimation(show_fab2);
        fab_edit.setClickable(true);
    }

    public void hideMenu() {
        isFABOpen = false;
        //For Fab1 menu
        FrameLayout.LayoutParams hide_layoutParams_fab_1 = (FrameLayout.LayoutParams) fab_delete.getLayoutParams();
        hide_layoutParams_fab_1.rightMargin -= (int) (fab_delete.getWidth() * 1.7);
        hide_layoutParams_fab_1.bottomMargin -= (int) (fab_delete.getHeight() * 0.25);
        fab_delete.setLayoutParams(hide_layoutParams_fab_1);
        fab_delete.startAnimation(hide_fab1);
        fab_delete.setClickable(false);

        //For Fab2 menu
        FrameLayout.LayoutParams hide_layoutParams_fab_2 = (FrameLayout.LayoutParams) fab_edit.getLayoutParams();
        hide_layoutParams_fab_2.rightMargin -= (int) (fab_edit.getWidth() * 0.25);
        hide_layoutParams_fab_2.bottomMargin -= (int) (fab_edit.getHeight() * 1.7);
        fab_edit.setLayoutParams(hide_layoutParams_fab_2);
        fab_edit.startAnimation(hide_fab2);
        fab_edit.setClickable(true);
    }

    /**
     * Set positive click listener for dialog
     */
    public DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            finish();
        }
    };
    /**
     * Set Negative click listener for dialog
     */
    public DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            dialogInterface.dismiss();
        }
    };

}
