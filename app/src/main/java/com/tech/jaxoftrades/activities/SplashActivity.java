package com.tech.jaxoftrades.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.Global;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        try {
            if (Global.getPreference("IsLogin", false)) {
                Intent iHome = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(iHome);
                finish();
                Global.activityTransition(SplashActivity.this);

            } else {
                Intent iLogin = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(iLogin);
                finish();
                Global.activityTransition(SplashActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
