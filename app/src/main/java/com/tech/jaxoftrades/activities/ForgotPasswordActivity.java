package com.tech.jaxoftrades.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {

    private android.widget.EditText etForgotEmail;
    private android.widget.Button btnForgotEmail;

    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    Bundle bForgotEmail;
    String strEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        apiFunctions=new ApiFunctions(this,this);
        bForgotEmail = new Bundle(getIntent().getExtras());
        strEmail = bForgotEmail.getString("ForgotEmail");
        this.btnForgotEmail = (Button) findViewById(R.id.btnForgotEmail);
        this.etForgotEmail = (EditText) findViewById(R.id.etForgotEmail);

        btnForgotEmail.setOnClickListener(this);
        etForgotEmail.setText(strEmail);
        etForgotEmail.setSelection(etForgotEmail.getText().length());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnForgotEmail:
                try {
                    String email = etForgotEmail.getText().toString();
                    if (Global.isNetworkAvailable(this)) {
                        if (!TextUtils.isEmpty(email)) {
                            AppDialog.showProgressDialog(ForgotPasswordActivity.this);
                            apiFunctions.forgotPassword(Api.MainUrl + Api.ActionTmForgotPassword, email);
                        } else {
                            etForgotEmail.setError(getString(R.string.ForgotPasswordError));
                        }
                    } else {
                        AppDialog.noNetworkDialog(this, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Intent iResetPass = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
//                startActivity(iResetPass);
//                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        AppDialog.dismissProgressDialog();
        Log.e("Forgot Password Success" + responseString);
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {

                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                    //display error message as per response

                } else {
                    //when response code not match default message will display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                            AppDialog.showAlertDialog(ForgotPasswordActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            finish();
                                        }
                                    });

                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(ForgotPasswordActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(ForgotPasswordActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        AppDialog.dismissProgressDialog();
        Log.e("Forgot Password Error" + errorMessage);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(ForgotPasswordActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

    }
}
