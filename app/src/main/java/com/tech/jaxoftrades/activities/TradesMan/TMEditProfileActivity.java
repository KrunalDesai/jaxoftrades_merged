package com.tech.jaxoftrades.activities.TradesMan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.HomeActivity;
import com.tech.jaxoftrades.adapter.TradesManProfile.TMEditProfileAdapter;
import com.tech.jaxoftrades.utils.PageIndicator.CirclePageIndicator;

public class TMEditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private android.support.v4.view.ViewPager vpTMEditProfile;
    private android.widget.Button btnProNext;
    private com.tech.jaxoftrades.utils.PageIndicator.CirclePageIndicator indicatorPro;

    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;
    ImageView tbBack;
    //Related to adapter
    TMEditProfileAdapter tmEditProfileAdapter;

    //Circle indicator
    CirclePageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmedit_profile);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbTitle.setText("");
        tbBack.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        this.indicatorPro = (CirclePageIndicator) findViewById(R.id.indicatorPro);
        this.btnProNext = (Button) findViewById(R.id.btnProNext);
        this.vpTMEditProfile = (ViewPager) findViewById(R.id.vpTMEditProfile);
        //circle indicator
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicatorPro);

        //set Adapter to view pager
        tmEditProfileAdapter = new TMEditProfileAdapter(this, getSupportFragmentManager(), 5);
        vpTMEditProfile.setAdapter(tmEditProfileAdapter);
        mIndicator.setViewPager(vpTMEditProfile
        );
        btnProNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.btnProNext:
                if (vpTMEditProfile.getCurrentItem() + 1 == 5) {

                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    vpTMEditProfile.setCurrentItem(vpTMEditProfile.getCurrentItem() + 1, true);
                }

                break;
            default:
                break;
        }
    }
}
