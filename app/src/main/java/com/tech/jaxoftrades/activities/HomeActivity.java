package com.tech.jaxoftrades.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.LeftMenuAdapter;
import com.tech.jaxoftrades.fragments.LeftMenu.GalleryFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.HistoryFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.HomeFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.ProfileFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.ProjectsFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.RatingsFragment;
import com.tech.jaxoftrades.fragments.LeftMenu.SettingsFragment;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.listener.RecyclerItemClickListener;
import com.tech.jaxoftrades.model.Common.LeftMenuMain;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle;

    //Related To Header
    SimpleDraweeView sdTMUserProfile;
    TextView tvTMUserName;

    //Related To Adapter
    LeftMenuAdapter leftMenuAdapter;
    LeftMenuMain leftMenuMain;
    ArrayList<LeftMenuMain> leftMenuMainArrayList;

    //Related To Activity
    RecyclerView rvLeftMenu;
    TextView tvLogout;

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbTitle.setText(getString(R.string.TMLMHome));
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);


        //set Default Fragment Here
        fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.TMContainer, fragment)
                .commit();

        //Related To Activity
        tvLogout = (TextView) findViewById(R.id.tvLogout);
        rvLeftMenu = (RecyclerView) findViewById(R.id.rvLeftMenu);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        rvLeftMenu.setLayoutManager(mLayoutManager1);

        tvLogout.setOnClickListener(this);
        leftMenuMainArrayList = new ArrayList<>();
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMHome),R.drawable.ic_home));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMProfile),R.drawable.ic_profile));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMHistory),R.drawable.ic_history));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMRatings),R.drawable.ic_ratting));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMProjects),R.drawable.ic_projects));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMGallery),R.drawable.ic_gallery));
        leftMenuMainArrayList.add(new LeftMenuMain(getString(R.string.TMLMSettings),R.drawable.ic_settings));

        leftMenuAdapter = new LeftMenuAdapter(leftMenuMainArrayList, getApplicationContext());
        rvLeftMenu.setAdapter(leftMenuAdapter);

        //Related To Left Menu
        rvLeftMenu.addOnItemTouchListener(new RecyclerItemClickListener(this,
                new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                leftMenuMain = leftMenuMainArrayList.get(position);


                try {
                    if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMHome))) {
                        changeFragment(new HomeFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMProfile))) {
                        changeFragment(new ProfileFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    }else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMHistory))) {
                        changeFragment(new HistoryFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMRatings))) {
                        changeFragment(new RatingsFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMProjects))) {
                        changeFragment(new ProjectsFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMGallery))) {
                        changeFragment(new GalleryFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    } else if (leftMenuMain.getStrMenuTitle().equals(getString(R.string.TMLMSettings))) {
                        changeFragment(new SettingsFragment());
                        DrawerLayout drawer_home = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer_home.closeDrawer(GravityCompat.START);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    void changeFragment(Fragment fragment) {
        try {
            String backStateName = fragment.getClass().getName();
            String fragmentTag = backStateName;

            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);


            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                fragmentTransaction.replace(R.id.TMContainer, fragment, fragmentTag);
                fragmentTransaction.addToBackStack(backStateName);
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvLogout:
                Global.clearPreferences();
                startActivity(new Intent(this,LoginActivity.class));
                Global.activityTransition(HomeActivity.this);

                break;
            default:
                break;
        }
    }
}
