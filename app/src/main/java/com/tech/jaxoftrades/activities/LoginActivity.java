package com.tech.jaxoftrades.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {

    private android.widget.EditText etLoginEmail;
    private android.widget.EditText etLoginPass;
    private android.widget.Button btnLogin;
    private android.widget.TextView tvLoginForgot;
    private android.widget.TextView tvLoginAccount;
    private android.support.design.widget.TextInputLayout etLoginTIPass;

    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        apiFunctions = new ApiFunctions(this, this);
        this.etLoginTIPass = (TextInputLayout) findViewById(R.id.etLoginTIPass);
        this.etLoginEmail = (EditText) findViewById(R.id.etLoginEmail);
        this.etLoginPass = (EditText) findViewById(R.id.etLoginPass);
        this.tvLoginAccount = (TextView) findViewById(R.id.tvLoginAccount);
        this.tvLoginForgot = (TextView) findViewById(R.id.tvLoginForgot);
        this.btnLogin = (Button) findViewById(R.id.btnLogin);

        etLoginEmail.setText("krunal@tradesman.com");
        etLoginPass.setText("123456");

        btnLogin.setOnClickListener(this);
        tvLoginAccount.setOnClickListener(this);
        tvLoginForgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                try {
                    String email = etLoginEmail.getText().toString();
                    String password = etLoginPass.getText().toString();
                    if (Global.isNetworkAvailable(this)) {
                        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
                            AppDialog.showProgressDialog(LoginActivity.this);
                            apiFunctions.userLogin(Api.MainUrl + Api.ActionLogin, email, password);
                        } else {
                            AppDialog.showAlertDialog(LoginActivity.this, null, getString(R.string.LoginError),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    } else {
                        AppDialog.noNetworkDialog(this, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                    }
                    /*enable for direct login*/
//                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                    startActivity(intent);
//                    finish();
//                    Global.activityTransition(LoginActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.tvLoginForgot:
                Intent iForgot = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                iForgot.putExtra("ForgotEmail",etLoginEmail.getText().toString());
                startActivity(iForgot);
                break;
            case R.id.tvLoginAccount:
                Intent iSelection = new Intent(LoginActivity.this, SelectionActivity.class);
                startActivity(iSelection);
                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        AppDialog.dismissProgressDialog();
        Log.e("Login Success" + responseString);
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Api.data);
                    if (jsonArray.length() > 0) {
                        userResponse = gson.fromJson(jsonArray.get(0).toString(), UserResponse.class);
                        strUserResponse = gson.toJson(userResponse, UserResponse.class);
                    }

                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                    //display error message as per response

                } else {
                    //when response code not match default message will display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                            Global.storePreference("UserResponseData", strUserResponse);
                            Global.storePreference("IsLogin", true);
                            Toast.makeText(LoginActivity.this, apiResponse.getMessage(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Global.activityTransition(LoginActivity.this);
                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(LoginActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(LoginActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        AppDialog.dismissProgressDialog();
        Log.e("Login Error" + errorMessage);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(LoginActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

    }
}