package com.tech.jaxoftrades.activities.ProfileMenus;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONObject;

public class ProfileWriteTestimoniActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {


    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle, tbSave;
    ImageView tbBack;

    EditText etTMTestimonial;


    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_write_testimoni);

        apiFunctions = new ApiFunctions(this, this);
        //getting data
        strUserResponse = Global.getPreference("UserResponseData", "");
        userResponse = gson.fromJson(strUserResponse.toString(), UserResponse.class);

//        Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbSave = (TextView) toolbar.findViewById(R.id.tbSave);
        tbTitle.setText(getString(R.string.TmProWriteTestimonial));
        tbBack.setVisibility(View.VISIBLE);
        tbSave.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        tbSave.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        etTMTestimonial = (EditText) findViewById(R.id.etTMTestimonial);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.tbSave:
//                try {
//                    if (Global.isNetworkAvailable(this)) {
//
//                        if (userResponse.getDATA().get(0).getTradesmenId() != null && !userResponse.getDATA().get(0).getTradesmenId().equals("")) {
//                            String type = Constants.TYPE_TRADESMEN;
//                            String tradesmen_id = userResponse.getDATA().get(0).getTradesmenId();
//                            String t_text = etTMTestimonial.getText().toString();
//                            AppDialog.showProgressDialog(ProfileWriteTestimoniActivity.this);
//                            apiFunctions.tmTestimonial(Api.MainUrl + Api.ActionTmTestimonial, type, tradesmen_id, t_text);
//                        }
//
//                    } else {
//                        AppDialog.noNetworkDialog(this, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        });
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                finish();
                break;

            default:
                break;
        }

    }


    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        AppDialog.dismissProgressDialog();
        Log.e("Tradesman Testimonial  Error" + responseString);
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {


                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                    //display error message as per response

                } else {
                    //when response code not match default message will  display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                            AppDialog.showAlertDialog(ProfileWriteTestimoniActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(ProfileWriteTestimoniActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(ProfileWriteTestimoniActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(final String errorMessage) {
        AppDialog.dismissProgressDialog();
        Log.e("Tradesman Testimonial  Error" + errorMessage);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(ProfileWriteTestimoniActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

    }
}

