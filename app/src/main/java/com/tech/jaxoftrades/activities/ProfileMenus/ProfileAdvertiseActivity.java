package com.tech.jaxoftrades.activities.ProfileMenus;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.adapter.TradesManProfile.ProfileAdvertisementAdapter;
import com.tech.jaxoftrades.global.AppDialog;
import com.tech.jaxoftrades.global.Global;
import com.tech.jaxoftrades.global.Log;
import com.tech.jaxoftrades.listener.OnApiCallListener;
import com.tech.jaxoftrades.model.Common.ApiResponse;
import com.tech.jaxoftrades.model.Response.ProfileAdvertisementItem;
import com.tech.jaxoftrades.model.Response.UserResponse;
import com.tech.jaxoftrades.webServices.Api;
import com.tech.jaxoftrades.webServices.ApiFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileAdvertiseActivity extends AppCompatActivity implements View.OnClickListener, OnApiCallListener {


    //Related To Custom Toolbar
    Toolbar toolbar;
    TextView tbTitle, tbSave;
    ImageView tbBack;

    RecyclerView rvAdvertisement;
    TextView tvAdvertiseEmpty;

    //Related to api
    ApiFunctions apiFunctions;
    ApiResponse apiResponse;
    Gson gson = new Gson();
    UserResponse userResponse;
    String strUserResponse;

    ArrayList<ProfileAdvertisementItem> profileAdvertisementItemArrayList;
    ProfileAdvertisementAdapter profileAdvertisementAdapter;
    ProfileAdvertisementItem profileAdvertisementItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_advertise);

        apiFunctions = new ApiFunctions(this, this);
        //Related To Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tbTitle = (TextView) toolbar.findViewById(R.id.tbTitle);
        tbBack = (ImageView) toolbar.findViewById(R.id.tbBack);
        tbSave = (TextView) toolbar.findViewById(R.id.tbSave);
        tbTitle.setText(getString(R.string.TmProAdvertiseProfile));
        tbBack.setVisibility(View.VISIBLE);
        tbSave.setVisibility(View.VISIBLE);
        tbBack.setOnClickListener(this);
        tbSave.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //getting data
        strUserResponse = Global.getPreference("UserResponseData", "");
        userResponse = gson.fromJson(strUserResponse.toString(), UserResponse.class);

        rvAdvertisement = (RecyclerView) findViewById(R.id.rvAdvertisement);
        tvAdvertiseEmpty = (TextView) findViewById(R.id.tvAdvertiseEmpty);

        profileAdvertisementItemArrayList=new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this);
        rvAdvertisement.setLayoutManager(mLayoutManager1);
        try {
            if (Global.isNetworkAvailable(this)) {
                //AppDialog.showProgressDialog(this);
                //apiFunctions.getHistoryList(Api.MainUrl + Api.ActionTmHistory);

                profileAdvertisementItemArrayList.add(new ProfileAdvertisementItem());
                profileAdvertisementItemArrayList.add(new ProfileAdvertisementItem());
                profileAdvertisementItemArrayList.add(new ProfileAdvertisementItem());
                profileAdvertisementItemArrayList.add(new ProfileAdvertisementItem());
                profileAdvertisementAdapter = new ProfileAdvertisementAdapter(profileAdvertisementItemArrayList,ProfileAdvertiseActivity.this);
                rvAdvertisement.setAdapter(profileAdvertisementAdapter);
            } else {
                AppDialog.noNetworkDialog(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbBack:
                finish();
                break;
            case R.id.tbSave:
                finish();
                break;

            default:
                break;
        }

    }

    @Override
    public void onSuccess(int responseCode, String responseString, String requestType) {
        AppDialog.dismissProgressDialog();
        Log.e("Profile Advertisement Success" + responseString);
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(responseString);
            if (!TextUtils.isEmpty(responseString)) {
                apiResponse = gson.fromJson(jsonObject.toString(), ApiResponse.class);
                if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Api.data);
                    if (jsonArray.length() > 0) {
                        profileAdvertisementItem = gson.fromJson(jsonArray.get(0).toString(), ProfileAdvertisementItem.class);
                        profileAdvertisementItemArrayList.add(profileAdvertisementItem);
                    }

                } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {

                    //display error message as per response

                } else {
                    //when response code not match default message will display
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseOk)) {

                            if (!profileAdvertisementItemArrayList.isEmpty() || profileAdvertisementItemArrayList.size() < 0) {
                                rvAdvertisement.setVisibility(View.VISIBLE);
                                tvAdvertiseEmpty.setVisibility(View.GONE);
                                profileAdvertisementAdapter = new ProfileAdvertisementAdapter(profileAdvertisementItemArrayList,ProfileAdvertiseActivity.this);
                                rvAdvertisement.setAdapter(profileAdvertisementAdapter);
                            } else {
                                rvAdvertisement.setVisibility(View.GONE);
                                tvAdvertiseEmpty.setVisibility(View.VISIBLE);
                            }

                        } else if (apiResponse.getSTATUS().equalsIgnoreCase(Api.StrResponseError)) {
                            AppDialog.showAlertDialog(ProfileAdvertiseActivity.this, null, apiResponse.getMessage(),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                        } else {
                            AppDialog.showAlertDialog(ProfileAdvertiseActivity.this, null, getResources().getString(R.string.something_wrong_txt),
                                    getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                        }
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(final String errorMessage) {
        AppDialog.dismissProgressDialog();
        Log.e("Profile Advertisement   Failure" + errorMessage);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppDialog.showAlertDialog(ProfileAdvertiseActivity.this, null, errorMessage,
                        getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

    }
}
