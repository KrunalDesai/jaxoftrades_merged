package com.tech.jaxoftrades.adapter.Tabs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.ViewHolder> {
    ArrayList<String> stringArrayList;
    Context context;
    String c;


    public JobsAdapter(ArrayList<String> stringArrayList, Context context) {
        this.stringArrayList = stringArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 1) {
            holder.tvJRCompleted.setVisibility(View.GONE);
            holder.tvJRDown.setVisibility(View.GONE);
            holder.tvJRRating.setVisibility(View.VISIBLE);
        }
        else {
            holder.llJRRating.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView tvJRCompleted,
                tvJRDown,
                tvJRRating;
        LinearLayout llJRRating;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvJRCompleted = (TextView) view.findViewById(R.id.tvJRCompleted);
            tvJRDown = (TextView) view.findViewById(R.id.tvJRDown);
            tvJRRating = (TextView) view.findViewById(R.id.tvJRRating);
            llJRRating = (LinearLayout) view.findViewById(R.id.llJRRating);


        }


    }
}
