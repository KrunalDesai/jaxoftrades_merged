package com.tech.jaxoftrades.adapter.TradesManProfile;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.TradesManProfile.TmAboutYouFragment;
import com.tech.jaxoftrades.fragments.TradesManProfile.TmDailyAvaiFragment;
import com.tech.jaxoftrades.fragments.TradesManProfile.TmDefaultLocationFragment;
import com.tech.jaxoftrades.fragments.TradesManProfile.TmEditProfileMainFragment;
import com.tech.jaxoftrades.fragments.TradesManProfile.TmSkillsFragment;

/**
 * Created by arbaz on 22/5/17.
 */

public class TMEditProfileAdapter extends FragmentStatePagerAdapter {

    Context context;
    int viewCount;

    public TMEditProfileAdapter(Context context, FragmentManager fm, int viewCount) {
        super(fm);
        this.context = context;
        this.viewCount = viewCount;
    }


    @Override
    public int getCount() {
        return viewCount;
    }

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                TmEditProfileMainFragment tmEditProfileMainFragment = new TmEditProfileMainFragment();
                return tmEditProfileMainFragment;
            case 1:
                TmAboutYouFragment tmAboutYouFragment= new TmAboutYouFragment();
                return tmAboutYouFragment;
            case 2:
                TmSkillsFragment skillsFragment = new TmSkillsFragment();
                return skillsFragment;
            case 3:
                TmDailyAvaiFragment tmDailyAvaiFragment= new TmDailyAvaiFragment();
                return tmDailyAvaiFragment;
            case 4:
                TmDefaultLocationFragment tmDefaultLocationFragment= new TmDefaultLocationFragment();
                return tmDefaultLocationFragment;

            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = " ";
        switch (position) {
            case 0:
                title = context.getString(R.string.TMEPMainTitle);
                break;
            case 1:
                title = context.getString(R.string.TMEPAboutYouTitle);
                break;
            case 2:
                title = context.getString(R.string.TMEPSkillsTitle);
                break;
            case 3:
                title = context.getString(R.string.TMEPDailyAvaiTitle);
                break;
            case 4:
                title = context.getString(R.string.TMEPDefLocationTitle);
                break;

        }

        return title;
    }
}
