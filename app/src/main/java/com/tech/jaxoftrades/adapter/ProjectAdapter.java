package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
    ArrayList<String> stringArrayList;
    Context context;
    String c;


    public ProjectAdapter(ArrayList<String> stringArrayList, Context context) {
        this.stringArrayList = stringArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView tvJRCompleted,
                tvJRDown,
                tvJRRating;
        LinearLayout llJRRating;


        public ViewHolder(View view) {
            super(view);
            mView = view;




        }


    }
}
