package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.fragments.DashboardTabs.AvailabilityFragment;
import com.tech.jaxoftrades.fragments.DashboardTabs.IncomingRequestsFragment;
import com.tech.jaxoftrades.fragments.DashboardTabs.JobsFragment;

/**
 * Created by arbaz on 25/5/17.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {

    int tabCount;
    Context context;

    public TabsAdapter(Context context, FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        this.context = context;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title = " ";
        switch (position) {
            case 0:
                title = context.getString(R.string.TmDBIncomingRequests);
                break;
            case 1:
                title = context.getString(R.string.TmDBJobs);
                break;
            case 2:
                title = context.getString(R.string.TmDBAvailability);
                break;

        }

        return title;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                IncomingRequestsFragment incomingRequestsFragment = new IncomingRequestsFragment();
                return incomingRequestsFragment;

            case 1:
                JobsFragment jobsFragment = new JobsFragment();
                return jobsFragment;
            case 2:
                AvailabilityFragment availabilityFragment = new AvailabilityFragment();
                return availabilityFragment;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
