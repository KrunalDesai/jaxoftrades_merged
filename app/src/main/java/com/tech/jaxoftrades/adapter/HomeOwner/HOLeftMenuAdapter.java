package com.tech.jaxoftrades.adapter.HomeOwner;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeOwnerLeftMenuMain;

import java.util.ArrayList;


public class HOLeftMenuAdapter extends RecyclerView.Adapter<HOLeftMenuAdapter.ViewHolder> {
    ArrayList<HomeOwnerLeftMenuMain> leftHOMenuMainArrayList;
    Context context;
    HomeOwnerLeftMenuMain HOleftMenuMain;


    public HOLeftMenuAdapter(ArrayList<HomeOwnerLeftMenuMain> leftHOMenuMainArrayList, Context context) {
        this.leftHOMenuMainArrayList = leftHOMenuMainArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ho_left_menu_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HOleftMenuMain = leftHOMenuMainArrayList.get(position);
        try {
            holder.tvHOLeftMenu.setText(HOleftMenuMain.getStrHOMenuTitle());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leftHOMenuMainArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView tvHOLeftMenu;
       

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvHOLeftMenu = (TextView) view.findViewById(R.id.tvHOLeftMenu);


        }
    }
}
