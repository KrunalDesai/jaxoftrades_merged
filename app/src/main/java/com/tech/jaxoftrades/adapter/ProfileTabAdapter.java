package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tech.jaxoftrades.fragments.Profile.ProfileTabFragment;
import com.tech.jaxoftrades.model.Common.ProfileTabMain;

import java.util.ArrayList;

/**
 * Created by arbaz on 19/6/17.
 */

public class ProfileTabAdapter extends FragmentStatePagerAdapter {

    int tabCount;
    Context context;
    ArrayList<ProfileTabMain> profileTabMainArrayList;
    ProfileTabMain profileTabMain;
    long baseId = 0;

    public ProfileTabAdapter(Context context, FragmentManager fm, ArrayList<ProfileTabMain> profileTabMainArrayList) {
        super(fm);
        this.profileTabMainArrayList = profileTabMainArrayList;
        this.context = context;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return profileTabMainArrayList.get(position).getTitle();
    }

    @Override
    public Fragment getItem(int position) {
//        switch (position) {
//            case 0:
//                IncomingRequestsFragment incomingRequestsFragment = new IncomingRequestsFragment();
//                return incomingRequestsFragment;
//
//            case 1:
//                JobsFragment jobsFragment = new JobsFragment();
//                return jobsFragment;
//            case 2:
//                AvailabilityFragment availabilityFragment = new AvailabilityFragment();
//                return availabilityFragment;
//
//            default:
//                return null;
//        }
//
//    }

        return new ProfileTabFragment().newInstance(profileTabMainArrayList.get(position).getCatID());
    }

    public void notifyChangeInPosition(int n) {
        // shift the ID returned by getItemId outside the range of all previous fragments
        baseId += getCount() + n;
    }

    @Override
    public int getCount() {
        return profileTabMainArrayList.size();
    }

    public void removeTabPage(int position) {
        if (!profileTabMainArrayList.isEmpty() && position < profileTabMainArrayList.size()) {
            profileTabMainArrayList.remove(position);
            notifyDataSetChanged();
        }
    }

}