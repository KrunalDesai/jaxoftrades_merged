package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.Common.CompanyProfileListMain;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class CompanyProfileListAdapter extends RecyclerView.Adapter<CompanyProfileListAdapter.ViewHolder> {
    ArrayList<CompanyProfileListMain> companyProfileListMainArrayList;
    Context context;
    CompanyProfileListMain companyProfileListMain;


    public CompanyProfileListAdapter(ArrayList<CompanyProfileListMain> companyProfileListMainArrayList, Context context) {
        this.companyProfileListMainArrayList = companyProfileListMainArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.company_profile_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        companyProfileListMain = companyProfileListMainArrayList.get(position);
        try {
            if (companyProfileListMain!=null) {

                    holder.tvCProfile.setText(companyProfileListMain.getItemName());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return companyProfileListMainArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;

        public TextView tvCProfile;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvCProfile = (TextView) view.findViewById(R.id.tvCProfile);



        }
    }
}
