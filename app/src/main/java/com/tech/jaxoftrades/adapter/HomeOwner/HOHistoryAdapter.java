package com.tech.jaxoftrades.adapter.HomeOwner;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs.TradesmenProfileToHOActivity;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeownerHistory;

import java.util.List;


/**
 * Created by Krunal on 2/27/2017.
 */




public class HOHistoryAdapter extends RecyclerView.Adapter<HOHistoryAdapter.MyViewHolder> {

    private Context mContext;

    private List<HomeownerHistory> homeownerHistoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView my_image_view;
        public TextView tvfnameLname, tvWorkdate, tvStatusTitle,tvWorkStatus,tvRatingTitle,tvAvgRating;
        public RatingBar rtbClientRating;
        public Button btnEditRating,btnHOWorkRating;
        View mRootView;

        public MyViewHolder(View view) {
            super(view);
            tvfnameLname = (TextView) view.findViewById(R.id.tvfnameLname);
            tvWorkdate = (TextView) view.findViewById(R.id.tvWorkdate);
            tvStatusTitle = (TextView) view.findViewById(R.id.tvStatusTitle);
            tvWorkStatus = (TextView) view.findViewById(R.id.tvWorkStatus);
            tvRatingTitle = (TextView) view.findViewById(R.id.tvRatingTitle);
            tvAvgRating = (TextView) view.findViewById(R.id.tvAvgRating);
            my_image_view = (SimpleDraweeView) view.findViewById(R.id.my_image_view);
            rtbClientRating = (RatingBar) view.findViewById(R.id.rtbClientRating);
            btnEditRating =(Button) view.findViewById(R.id.btnEditRating);

            mRootView=view;


        }
    }


    public HOHistoryAdapter(Context mContext, List<HomeownerHistory> homeownerHistoryList) {
        this.mContext = mContext;
        this.homeownerHistoryList = homeownerHistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Fresco.initialize(mContext);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homeowner_history_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        HomeownerHistory movie = homeownerHistoryList.get(position);
        holder.tvfnameLname.setText(movie.getTitle());
        holder.tvWorkdate.setText(movie.getGenre());
        holder.tvStatusTitle.setText(movie.getYear());
holder.btnEditRating.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String item = String.valueOf(homeownerHistoryList.get(position).getTitle());
        Toast.makeText(mContext, item, Toast.LENGTH_SHORT).show();

        Intent i = new Intent(mContext, TradesmenProfileToHOActivity.class);
        mContext.startActivity(i);



    }
});
//        holder.mRootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String item = String.valueOf(homeownerHistoryList.get(position).getTitle());
//                Toast.makeText(mContext, item, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return homeownerHistoryList.size();
    }
}