package com.tech.jaxoftrades.adapter.TradesManProfile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.Response.ProfileAdvertisementItem;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class ProfileAdvertisementAdapter extends RecyclerView.Adapter<ProfileAdvertisementAdapter.ViewHolder> {
    ArrayList<ProfileAdvertisementItem> profileAdvertisementItemArrayList;
    Context context;
    String c;
    ProfileAdvertisementItem profileAdvertisementItem;

    public ProfileAdvertisementAdapter(ArrayList<ProfileAdvertisementItem> profileAdvertisementItemArrayList, Context context) {
        this.profileAdvertisementItemArrayList = profileAdvertisementItemArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.advertisement_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        profileAdvertisementItem = profileAdvertisementItemArrayList.get(position);
        if (profileAdvertisementItem != null) {
//            holder.tvTmPATitle.setText(profileAdvertisementItem.getJobType());
//            holder.tvTmPAText.setText(profileAdvertisementItem.getDailyTime());
            holder.btnTmPAPurchase.setTag(position);
            holder.btnTmPAPurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id= (int) v.getTag();


                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return profileAdvertisementItemArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView tvTmPATitle,
                tvTmPAText;
        Button btnTmPAPurchase;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvTmPATitle = (TextView) view.findViewById(R.id.tvTmPATitle);
            tvTmPAText = (TextView) view.findViewById(R.id.tvTmPAText);
            btnTmPAPurchase = (Button) view.findViewById(R.id.btnTmPAPurchase);

        }


    }
}
