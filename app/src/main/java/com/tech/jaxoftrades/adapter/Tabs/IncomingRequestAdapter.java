package com.tech.jaxoftrades.adapter.Tabs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.global.AppDialog;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class IncomingRequestAdapter extends RecyclerView.Adapter<IncomingRequestAdapter.ViewHolder> {
    ArrayList<String> stringArrayList;
    Context context;
    String c;


    public IncomingRequestAdapter(ArrayList<String> stringArrayList, Context context) {
        this.stringArrayList = stringArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.incoming_req_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View mView;


        LinearLayout llIRGreenLayoutTop, llIRReadDesNote, llIRAcceptNote, llIRRedDecline, llIRGreen, llIRWhite,
                llIRGreenLayoutBottomReadDes,
                llIRGreenLayoutBottomAccept;
        TextView tvIRReadDesTitle,
                tvIRAcceptTitle,
                tvIRTradesman,
                tvIRLocation,
                tvIRTime,
                tvIRReadDes,
                tvIRAccept,
                tvIRDecline,
                tvIRStartWork,
                tvIRDownJob,
                tvIRClear;

        ImageView ivIRReadDesNoteCancel;


        public ViewHolder(View view) {
            super(view);
            mView = view;

            llIRGreenLayoutTop = (LinearLayout) view.findViewById(R.id.llIRGreenLayoutTop);
            llIRReadDesNote = (LinearLayout) view.findViewById(R.id.llIRReadDesNote);
            llIRAcceptNote = (LinearLayout) view.findViewById(R.id.llIRAcceptNote);
            llIRRedDecline = (LinearLayout) view.findViewById(R.id.llIRRedDecline);
            llIRGreen = (LinearLayout) view.findViewById(R.id.llIRGreen);
            llIRWhite = (LinearLayout) view.findViewById(R.id.llIRWhite);
            llIRGreenLayoutBottomReadDes = (LinearLayout) view.findViewById(R.id.llIRGreenLayoutBottomReadDes);
            llIRGreenLayoutBottomAccept = (LinearLayout) view.findViewById(R.id.llIRGreenLayoutBottomAccept);

            tvIRReadDesTitle = (TextView) view.findViewById(R.id.tvIRReadDesTitle);
            tvIRAcceptTitle = (TextView) view.findViewById(R.id.tvIRAcceptTitle);
            tvIRTradesman = (TextView) view.findViewById(R.id.tvIRTradesman);
            tvIRLocation = (TextView) view.findViewById(R.id.tvIRLocation);
            tvIRTime = (TextView) view.findViewById(R.id.tvIRTime);
            tvIRReadDes = (TextView) view.findViewById(R.id.tvIRReadDes);
            tvIRAccept = (TextView) view.findViewById(R.id.tvIRAccept);
            tvIRDecline = (TextView) view.findViewById(R.id.tvIRDecline);
            tvIRStartWork = (TextView) view.findViewById(R.id.tvIRStartWork);
            tvIRDownJob = (TextView) view.findViewById(R.id.tvIRDownJob);
            tvIRClear = (TextView) view.findViewById(R.id.tvIRClear);

            ivIRReadDesNoteCancel = (ImageView) view.findViewById(R.id.ivIRReadDesNoteCancel);

            tvIRReadDes.setOnClickListener(this);
            ivIRReadDesNoteCancel.setOnClickListener(this);

            tvIRAccept.setOnClickListener(this);
            tvIRStartWork.setOnClickListener(this);

            tvIRDecline.setOnClickListener(this);
            tvIRClear.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvIRReadDes:
                    llIRWhite.setVisibility(View.VISIBLE);
                    llIRReadDesNote.setVisibility(View.VISIBLE);
                    tvIRReadDes.setVisibility(View.INVISIBLE);
                    break;
                case R.id.ivIRReadDesNoteCancel:
                    llIRWhite.setVisibility(View.GONE);
                    llIRReadDesNote.setVisibility(View.GONE);
                    tvIRReadDes.setVisibility(View.VISIBLE);
                    break;
                case R.id.tvIRAccept:
                    tvIRReadDesTitle.setVisibility(View.GONE);
                    tvIRAcceptTitle.setVisibility(View.VISIBLE);
                    tvIRTime.setVisibility(View.INVISIBLE);
                    llIRWhite.setVisibility(View.VISIBLE);
                    llIRReadDesNote.setVisibility(View.GONE);
                    llIRAcceptNote.setVisibility(View.VISIBLE);
                    llIRGreenLayoutBottomReadDes.setVisibility(View.GONE);
                    llIRGreenLayoutBottomAccept.setVisibility(View.VISIBLE);
                    break;
                case R.id.tvIRStartWork:
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppDialog.showAlertDialog(context, null, context.getString(R.string.TmDbIRBtnAcceptStartWorkDialog),
                                    context.getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            stringArrayList.clear();
                                            notifyDataSetChanged();
                                        }
                                    });
                        }
                    });
                    break;
                case R.id.tvIRDecline:
                    llIRGreen.setVisibility(View.GONE);
                    llIRRedDecline.setVisibility(View.VISIBLE);
                    break;
                case R.id.tvIRClear:
                    stringArrayList.clear();
                    notifyDataSetChanged();
                    break;

                default:
                    break;

            }
        }
    }
}
