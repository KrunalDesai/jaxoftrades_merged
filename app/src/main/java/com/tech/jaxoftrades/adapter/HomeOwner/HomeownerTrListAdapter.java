package com.tech.jaxoftrades.adapter.HomeOwner;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.activities.Homeowner.ViewTRProfileTabs.TradesmenProfileToHOActivity;
import com.tech.jaxoftrades.model.HomOwnerModel.HomeOwnerTrList;

import java.util.List;


/**
 * Created by Krunal on 2/27/2017.
 */


public class HomeownerTrListAdapter extends RecyclerView.Adapter<HomeownerTrListAdapter.MyViewHolder> {

    private Context mContext;

    private List<HomeOwnerTrList> homeownerTrLists;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView ivTradesmanListPic;
        public TextView tvDistance, tvListfnameLname, tvListCategory,tvListCompanyName,tvListAvailability,tvListAvgRating;
        public RatingBar rtbListRating;
        public Button btnRequest,btnViewProfile;
        View mRootView;

        public MyViewHolder(View view) {
            super(view);
            //initalizing text view
            tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            tvListfnameLname = (TextView) view.findViewById(R.id.tvListfnameLname);
            tvListCategory = (TextView) view.findViewById(R.id.tvListCategory);
            tvListCompanyName = (TextView) view.findViewById(R.id.tvListCompanyName);
            tvListAvailability = (TextView) view.findViewById(R.id.tvListAvailability);
            tvListAvgRating = (TextView) view.findViewById(R.id.tvListAvgRating);

            //imageview tradesman
            ivTradesmanListPic = (SimpleDraweeView) view.findViewById(R.id.ivTradesmanListPic);

            rtbListRating = (RatingBar) view.findViewById(R.id.rtbListRating);

            btnRequest =(Button) view.findViewById(R.id.btnRequest);
            btnViewProfile =(Button) view.findViewById(R.id.btnViewProfile);

            mRootView=view;


        }
    }


    public HomeownerTrListAdapter(Context mContext, List<HomeOwnerTrList> homeownerTrLists) {
        this.mContext = mContext;
        this.homeownerTrLists = homeownerTrLists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Fresco.initialize(mContext);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homeowner_tradesmen_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        HomeOwnerTrList trList = homeownerTrLists.get(position);
        holder.tvDistance.setText(trList.getDistance());
        holder.tvListfnameLname.setText(trList.getName());
        holder.tvListCategory.setText(trList.getCategory());
        holder.tvListCompanyName.setText(trList.getCompanyname());
        holder.tvListAvailability.setText(trList.getTimeAvail());
        holder.tvListAvgRating.setText(trList.getListAvgRating());

        holder.btnViewProfile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String item = String.valueOf(homeownerTrLists.get(position).getName());
        Toast.makeText(mContext, item, Toast.LENGTH_SHORT).show();

        Intent i = new Intent(mContext, TradesmenProfileToHOActivity.class);
        mContext.startActivity(i);

    }
});
//        holder.mRootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String item = String.valueOf(homeownerHistoryList.get(position).getTitle());
//                Toast.makeText(mContext, item, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return homeownerTrLists.size();
    }
}