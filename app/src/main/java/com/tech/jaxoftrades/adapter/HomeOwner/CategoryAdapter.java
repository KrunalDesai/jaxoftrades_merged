package com.tech.jaxoftrades.adapter.HomeOwner;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.HomOwnerModel.Category;

import java.util.List;


/**
 * Created by Krunal on 2/27/2017.
 */




public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder>  {

    private Context mContext;
    private List<Category> categoryList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
//        public RecyclerView mRecyclerView;

        View mRootView;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
//            mRecyclerView =(RecyclerView) view.findViewById(R.id.recycler_view);
            mRootView=view;

        }
    }


    public CategoryAdapter(Context mContext, List<Category> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Category category = categoryList.get(position);
        holder.title.setText(category.getCategoryName());

        Glide.with(mContext).load(category.getThumbnail()).into(holder.thumbnail);

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = String.valueOf(categoryList.get(position).getCategoryName());
//                Log.v("CategoryName",item);
                Toast.makeText(mContext, item, Toast.LENGTH_LONG).show();

            }
        });
//        holder.mRootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String item = String.valueOf(categoryList.get(position).getCategoryName());
//                Log.v("CategoryName",item);
//                Toast.makeText(mContext, item, Toast.LENGTH_LONG).show();
//            }
//        });
    }




    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}




