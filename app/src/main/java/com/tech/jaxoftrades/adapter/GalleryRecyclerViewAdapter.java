package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.Common.GalleryPhoto;

import java.util.List;



public class GalleryRecyclerViewAdapter extends RecyclerView.Adapter<GalleryRecyclerViewAdapter.ViewHolder> {

    private List<GalleryPhoto> mValues;
    private Context mContext;

    public GalleryRecyclerViewAdapter(Context mContext, List<GalleryPhoto> items) {
        mValues = items;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final SimpleDraweeView ivPhoto;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivPhoto = (SimpleDraweeView) view.findViewById(R.id.ivPhoto);
        }

    }

}
