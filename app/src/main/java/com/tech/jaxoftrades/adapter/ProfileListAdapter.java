package com.tech.jaxoftrades.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.jaxoftrades.R;
import com.tech.jaxoftrades.model.Common.ProfileListMain;

import java.util.ArrayList;

/**
 * Created by arbaz on 20/5/17.
 */

public class ProfileListAdapter extends RecyclerView.Adapter<ProfileListAdapter.ViewHolder> {
    ArrayList<ProfileListMain> profileListMainArrayList;
    Context context;
    ProfileListMain profileListMain;


    public ProfileListAdapter(ArrayList<ProfileListMain> profileListMainArrayList, Context context) {
        this.profileListMainArrayList = profileListMainArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        profileListMain = profileListMainArrayList.get(position);
        try {
            if (profileListMain!=null) {

                    holder.ivProIcon.setImageResource(profileListMain.getItemIcon());
                    holder.tvProfile.setText(profileListMain.getItemName());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return profileListMainArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public ImageView ivProIcon;
        public TextView tvProfile;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivProIcon = (ImageView) view.findViewById(R.id.ivProIcon);
            tvProfile = (TextView) view.findViewById(R.id.tvProfile);



        }
    }
}
